# `change-form`

This component provides a (complex if you desire so) for for a speific model record.

## Example usage

```
##route.js

...
  model(params) {
    return {
      model_name: 'sales/product',
      id: params.id,
      fieldsets: [
        {
          fields: [
            {
              class: "col-md-3 col-sm-6 col-xs-12",
              inputClass: "col-md-12 col-sm-9 col-xs-12",
              name: "picture",
              extra: {
                  clearable: true
              },
              horiClass: "col-md-12 col-sm-3 col-xs-12"
            }, {
              class: "col-md-9 col-sm-6 col-xs-12",
              widget: "fieldset",
              fields: [
                { name: "name" },
                {
                  name: "category",
                  extra: { allowAdd: true }
                },
                { name: "sale_type" },
                { name: "price"}
              ]
            }
          ],
          title: null
        }  
      ]
    }
  }


## templates.hbs

{{change-form
  model_name=model.model_name
  id=model.id
  fieldsets=model.fieldsets
  fetch_meta=true
}}
```

## Options

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *model_name* | **Yes** | | `'sales/product'` | the fully qualified model name |
| *id* | **Yes** | | `'new'` or `3` | The id of the record you want to edit. To add a new record, pass in `'new'` as `id` |
| fieldsets | **Yes** | | `[{title: null, fields: [{name: 'name'}]}]` | The structure of the form represented as a list of [`fieldset` widgets](./widgets.md#fieldset) |
| *defaults* | No | `{}` | A pojo with the default values to assign to a new record |
| *fetch_meta* | No | `false` | `true` | Whether or not `ember-cli-crudities` should use [ember-cli-dynamic-model](https://bitbucket.org/levit_scs/ember-cli-dynamic-model) to fetch more complete field definition for the top-level record. :warning: Note that, for now, `ember-cli-crudities` will **always** use `ember-cli-dynamic-model` to fetch field information about related models. |
| *saveTwice* | No | `false` | `true` | By default, the top-level model is saved once before saving each related model. If this option is turned on, **new** top-level records will be saved twice, once before and once after saving related models; **existing** records will be saved **once after** saving related models. This is usefull when a record has a `belongsTo` and a `hasMany` relationship both pointing towards the same model. |
| *sortableBy* | No | `undefined` | `'position'` | A field by which records of a model are ordered by default. If `sortableBy` is specified, this field will be added to the defaults with a value of `0` when creating new records |
| *changeListRoute* | No | `'index'` | `'list'` | The name of the list route. If specified, this route will be used to transition to after saving/deleting models. The route will receive 2 parameters: "app_name" and "model_name" as in `'sales'` and `'product'`. |
| *languages* | No | `undefined` | `['en-gb', 'fr-fr']` | An array of lower-case language-culture strings. If supplied, fields with `translated` set to `true` will be shown with a set of flags, clicking on a flag will switch the field to its localized value. |
| *cutom_actions* | No | `undefined` | See [actions](./actions.md) | Actions that can be performed on the current record. |

