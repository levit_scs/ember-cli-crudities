# Route mixin

**ember-cli-crudities** also provides a route mixin that performs the call to the 
backend api for you.

This mixin expects to receive as params at least an `app_name` and `model_name` (singular).

This mixin will return as model a promise hash with all the parameters passed to that
route as well as a `meta` object containing the results of an `OPTIONS` to the API route
for that `app_name`/`model_name`. It will also ensure the model already exists on the
frontend; if it doesn't, it will load the model definition from the backend.

## Usage

```
import Ember from 'ember';
import ChangeRouteMixin from 'ember-cli-crudities/mixins/change-route';

export default Ember.Route.extend(ChangeRouteMixin, {
});
```
