# Widgets

Widgets provided by **ember-cli-crudities** come in 4 forms:

- [`fieldset`](#fielset) widgets which are their own unique kind
- [`tabset`](#tabset) widgets which are also their own unique kind
- [`field widgets`](#field-widgets) used to represent single values
- [`collection widgets`](#collection-widgets) used to represent collections of related records as for `hasMany` relationships

## Options common to all widgets

| Option | Required? | Default | Description |
| ------ | --------- | ------- | ----------- |
| *name* | **Yes** (except for fieldset and tabset) | | The fieldname of the field this widgets represents |
| *widget* | No | `'text'` | The type of widget used to represent the field |
| *label* | No | `name` | The label used for the field |
| *class* | No | `undefined` | The classnames associated with the div surrounding the widget |
| *extra* | No | `{}` | Pojo containing options specific to a type of widget (mentioned further down this page as **Extras for widget**)|
| *readonly* | No | `false` | Whether the content of the widget is readonly or not |

## Extras common to all widgets


| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *display_condition* | No | `undefined` | `{ type: 'unless', operator: 'eq', property_path: 'product_type', value: 'p', required: true }` | A pojo describing the conditions under which this field should be displayed (see here-under for details about this object) |

## Display_condition options

Also used for other condition expressions

| Option | Required? | Default | Description |
| ------ | --------- | ------- | ----------- |
| *type* | No | `'if'` | The type of condition, valid types are `'if'` and `'unless'` |
| *operator* | **Yes** | | The operator used to compute the condition, see here-under for a list of valid operators |
| *property_path* | **Yes** | | A fully dotted path leading to a field from the current record. |
| *value* | **Yes** | | The value to which the field specified by `property_path` will be compared to |

### Display_condition operators

| Operator | Meaning |
| -------- | ------- |
| `'eq'` | `field_content === display_condition.value` |
| `'not-eq'` | `field_content !== display_condition.value` |
| `'gt'` | `field_content > display_condition.value` |
| `'gte'` | `field_content >= display_condition.value` |
| `'lt'` | `field_content < display_condition.value` |
| `'lte'` | `field_content <= display_condition.value` |
| `'in'` | `field_content` in supplied array `display_condition.value` |
| `'not-in'` | `field_content` not in supplied array `display_condition.value` |
| `'empty'` | result of calling `Ember.isEmpty` on `field_content` |
| `'not-empty'` | inversed result of calling `Ember.isEmpty` on `field_content` |


## Fieldset

A fieldset, a its name hints, represents a set of fields. It can have a label and can itself include other fieldsets as fields.

### Options specific to fieldset

| Option | Required? | Default | Description |
| ------ | --------- | ------- | ----------- |
| *fields* | **Yes** | | The list of fields this widget contains |
| *innerClass* | No | `'col-xs-12 row'` | The default classnames used for each field inside this fieldset. Each field can override this by providing its own `class` option |

## Tabset

A tabset is a set of tabs, each tab having a label and at least one field.

### Options specific to tabset

| Option | Required? | Default | Description |
| ------ | --------- | ------- | ----------- |
| *tabs* | **Yes** | | A set of tabs. Each tab having a required `label` will behave as a `fieldset` |

## Field widgets

### Options common to all field widgets

| Option | Required? | Default | Description |
| ------ | --------- | ------- | ----------- |
| *horiClass* | No | `'col-sm-3'` | The class used for the field's label |
| *inputClass* | No | `'col-sm-9'` | The class used for the field's input |
| *readonly* | No | `false` | whether or not the field should be displayed readonly |
| *placeholder* | No | `undefined` | A placeholder displayed inside the input |
| *required* | No | `false` | Whether this field is required or not |
| *translated* | No | `false` | Whether this field should be tranlatable in `change-form`'s (requires the `change-form` to be supplied with a `languages` option) |

### Text

A simple input field

### Url

A simple HTML5 url field

### Number

A simple HTML5 number field

### Color

A simple HTML5 color field

### Email

A simple HTML5 email field

### Tel

A simple HTML5 telephone field

### Date

A simple HTML5 date field

### Time

A simple HTML5 time field

### Duration

A widget to input time durations. It converts human-readable times into seconds.

By default the `duration` widget is debounced by 500 miliseconds as eg `3:` is
not a valid representation of time. This should give users enough time to input
full times.

#### Extras for duration

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *withSeconds* | No | `false` | `true` | Whether or not the duration widget should display seconds. |
| *debounce* | No | `500` | `750` | Duration of the debounce in ms |

### Datetime

A simple HTML5 datetime field

### Month

A simple HTML5 month field

### Week

A simple HTML5 week field

### Textarea

A textarea field

#### Extras for textarea

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *rows* | No | `4` | `7` | The value passed to the `rows` attribute of the textarea |

### Markdown

A textarea field with a `markdown` preview

#### Extras for markdown

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *rows* | No | `4` | `7` | The value passed to the `rows` attribute of the textarea. Will also be used to compute the height of the preview mode. |
| *sideBySide* | No | `false` | `true` | If set to `true` the markdown widget will be rendered in "sideBySide" mode with the result on the left and textarea on the right. |

### Checkbox

A checkbox field

### Select

A dropdown field

#### Extras for select

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *choices* | **Yes** | | `[{label: 'one', value: 1}]` | A list of `label`/`value` pairs representing the options of the dropdown |
| *allowOther* | No | `false` | `true` | If set to `true` this widget will propose an `Other` option. When selected a text input field will be rendered next to the original widget to allow entering a custom value. |

### File

A file field (will be base64-encoded to send to the api)

#### Extras for file

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *clearable* | No | `false` | `true` | Whether or not the file should be removable or not |

### Image

A file field with an image preview (will be base64-encoded to senf to the api)

#### Extras for image

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *clearable* | No | `false` | `true` | Whether or not the file should be removable or not |
| *width* | No | `'175px'` | `'100%'` | The maximum width of the preview |
| *height* | No | `'175px'` | `'350px'` | The maximum height of the preview |
| *tnWidth* | No | `'75px'` | `'50px'` | The maximum width of the preview in `readonly` mode (usualy used in `change-list`) |
| *tnHeight* | No | `'75px'` | `'100px'` | The maximum height of the preview in `readonly` mode (usualy used in `change-list`) |

### Foreignkey

A widget representing a `belongsTo` relationship.

#### Extras for foreignkey

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *related_model* | If `property_path` is not given | `undefined` | `'sales/category'` | The model name of the related model of the `belongsTo` relationship |
| *query* | No | `undefined` | `{is_active: true}` | A pojo representing the parameters to perform a `query` against the api if using `related_model`. When using `property_path` or `search='simple'`, this pojo will be used to filter against the resulting content. |
| *property_path* | If `related_model` is not given | `undefined` | `'employees'` | A fully dotted path leading to a collection of records from the current record. (ie: on a model with a `hasMany` to a model called `employees` and a `belongsTo` to the same model called `favourite`, `'employees'` could be used as `property_path` on the `foreignkey` representing `favourite`) |
| *label* | No | `'__str__'` | `'title'` | Name of the field on the related model used to display choices labels. |
| *autoComplete* | No | `false` | `true` | If enabled a searchbox will allow for auto-complete search inside the foreign-key drop-down |
| *search* | No | `'backend'` | `'simple'` | If set to 'simple' and autocomplete is enable, instead of querying the backend, search will happen by filtering results on the related model's *label* field. You should always use *simple* search with *property_path*. |
| *dynamicFilters* | No | `undefined` | A list of filter pojo's (see below) | A set of dynamic filters to be applied to list of available value depending on the value of another field. |
| *allowClearRequired* | No | `undefined` | `true` | When `true` user will be allowed to clear value of required field |
| *autoSetUnique* | No | `undefined` | `true` | When `true` value will be auto-selected when there is only one choice available |
| *isRelation* | No | `undefined` | `true` | Only used for widgets defining a `filter_field`: When `true`, this widget will be considered as a relationship instead of a value |
| *filterValueField* | No | `'id'` | `'name'` | Only used for widgets defining a `filter_field`: By default, the `'id'` of related models is used for queries to the backend. This can be altered by this field |

#### Dynamic filter pojo structure

| property | Required? | Sample value | Description |
| -------- | --------- | ------------ | ----------- |
| *property_path* | **Yes** | `'country.id'` | The fully-qualified dotted path to the property (on the current model) that wil be used to filter the available chocies. |
| *filter* | If the widget is using `related_model` | `'country_id'` | The name of the parameter that will be populated when executing the `query` to the backend. |
| *remote_property_path* | If the widget is using `property_path` or `search='simple'` | `'city.country.id'` | The fully-qualified dotted pathe to the property (on the model shown in the dropdown) that will be compared against. |

### Manytomany-lists

A dual list (available/selected) representing a many-to-many relationship towards another model.

### Extras for manytomany-lists


| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *related_model* | If `property_path` is not given | `undefined` | `'sales/category'` | The model name of the related model of the `belongsTo` relationship |
| *query* | No | `undefined` | `{is_active: true}` | A pojo representing the parameters to perform a `query` against the api. (Only works with `related_model`)|
| *property_path* | If `related_model` is not given | `undefined` | `'employees'` | A fully dotted path leading to a collection of records from the current record. (ie: on a model with a `hasMany` to a model called `employees` and a `belongsTo` to the same model called `favourite`, `'employees'` could be used as `property_path` on the `foreignkey` representing `favourite`) |
| *label* | No | `'__str__'` | `'title'` | Name of the field on the related model used to display choices labels. |
| *dynamicFilters* | No | `undefined` | A list of filter pojo's (see [Dynamic filter pojo structure](#dynamic-filter-pojo-structure) above) | A set of dynamic filters to be applied to list of available value depending on the value of another field. |

### Manytomany-select

A multi-select (checkboxes) dropdown representing a many-to-many relationship towards another model.

### Extras for manytomany-lists

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *related_model* | If `property_path` is not given | `undefined` | `'sales/category'` | The model name of the related model of the `belongsTo` relationship |
| *query* | No | `undefined` | `{is_active: true}` | A pojo representing the parameters to perform a `query` against the api. (Only works with `related_model`)|
| *property_path* | If `related_model` is not given | `undefined` | `'employees'` | A fully dotted path leading to a collection of records from the current record. |
| *label* | No | `'__str__'` | `'title'` | Name of the field on the related model used to display choices labels. |
| *autoComplete* | No | `false` | `true` | If enabled a searchbox will allow for auto-complete search inside the foreign-key drop-down |
| *search* | No | `'backend'` | `'simple'` | If set to 'simple' and autocomplete is enable, instead of querying the backend, search will happen by filtering results on the related model's *label* field. You should always use *simple* search with *property_path*. |
| *dynamicFilters* | No | `undefined` | A list of filter pojo's (see [Dynamic filter pojo structure](#dynamic-filter-pojo-structure) above) | A set of dynamic filters to be applied to list of available value depending on the value of another field. |

## Collection widgets

Collection widgets are used to represent a collection of records as in `hasMany` relationships.
By default, the conditional formatting associated to the target modelis picked up.

### Options common to all collection widgets

| Option | Required? | Default | Description |
| ------ | --------- | ------- | ----------- |
| *fields* | **Yes** | | A list of fields to be displayed in the related-model form |

### Extras common to all collection widgets

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *related_model* | **Yes** | | `'sales/category'` | The fully qualified model name to which records in this collection belong to |

### Tomany-slide

A sliding "carousel" representing a collection of records

### Tomany-table

A table representing a collection of records. Quite similar to `change-list`.

#### Extras for tomany-table

| Option | Required? | Default | Sample value | Description |
| ------ | --------- | ------- | ------------ | ----------- |
| *sortableBy* | No | `undefined` | `'position'` | The name of the field used to order records of the relationship. If this option is present, handles will be shown on the left of each record row, allowing to reorder the list |
| *allowOpen* | No | false | true | When `true` each record line will be editable "in full"; this is done by providing a pencil icon at the end of each record line, clicking that icon will open a popup with a full form for the record |

