# `change-list`

This component provides an editable model list.

## Example usage

```
## route.js

...
  model() {
    return {
      model_name: 'sales/category',
      label: 'category',
      app_name: 'sales',
      fields: [
        { 
          name: 'name',
          label: 'Name',
          widget: 'text',
        }
      ],
      list_display: ['name'],
      list_editable: ['name']
    };
  }


## template.hbs

{{change-list
  model_name=model.model_name
  label=model.label
  app_name=model.app_name
  fields=model.fields
  list_display=model.list_display
  list_editable=model.list_editable
}}
```

## Options

### model_name
**Required:** **Yes**
**Default:** 
**Sample value:** `'sales/product'`

Full model name.

### label
**Required:** **Yes**
**Default:** 
**Sample value:** `'product'`

Label of the model, used to compute the title of the list.

### app_name
**Required:** No
**Default:** `undefined` 
**Sample value:** `'sales'`

Used as parameter to the `changeFormRoute`

### fields
**Required:** **Yes**
**Default:** 
**Sample value:** `[{ name: 'name', label: 'Name', widget: 'text' }]`

A list of fields and their attributes. You must a least specify the name (as in
fieldname) for each field. For more information about the other parameters,
please see the [widgets section](./widgets).

### list_display
**Required:** **Yes**
**Default:** 
**Sample value:** `['name']`

The list of fieldnames that will be displayed for each record.

Depending on how it is acquired, the content of `fields` might have more fields
than `list_display`

**Advanced usage:** Instead of field names, `list_display` can receive pojo's
similar to the ones received by `fields`. Those pojo's can override properties
of a known field (eg: different label or widget) or define an unknown field
used only as `display_field` (eg: a computed property)

### list_editable
**Required:** No
**Default:** `[]` 
**Sample value:** `['name']`

The list of fields that should be editable directly in the records list.

### filter_fields
**Required:** No
**Default:** `[]` 
**Sample value:** `['status', 'category_id']`

The list of fields on which it is possible to filter the list of records on
exact values.

:warning: For `belongsTo`'s you can also use the form "fieldname_id" depending
on your backend requirements. This will be sent to the api as `?fieldname=value`.

**Advanced usage:** Instead of field names, `list_display` can receive pojo's
similar to the ones received by `fields`. Those pojo's can override properties
of a known field (eg: different label or widget) or define an unknown field
used only as `display_field` (eg: a computed property).

:warning: if you define a `filter_field` (via a pojo) that represents a
relationship. It is advised to:

- **not** use the suffix `_id`
- include `isRelation: true` in the `extra` property of the pojo

### search_enabled
**Required:** No
**Default:** `false` 
**Sample value:** `true`

Wheather or not a search field should be displayed (will be sent to the api
as `?search=seachValue`)

### ordering_fields
**Required:** No
**Default:** `[]` 
**Sample value:** `['name', 'created']`

A list of fields by which the list should be made sortable. This is an api
operation and the information will be sent to the api as `?ordering=(-)value`

### sortableBy
**Required:** No
**Default:** `'id'` 
**Sample value:** `'position'`

A positive integer field by which the record list is orderable. If specified,
a handle will be displayed on the left of each record in the list to allow
re-ordering

### changeFormRoute
**Required:** No
**Default:** `undefined` 
**Sample value:** `'form'`

The name of the route reponsible for record edition/addition. This route will
be passed 3 arguments: "app_name", "model_name" and id as in `'sales'`,
`'category'` and `3`

### cutom_actions
**Required:** No
**Default:** `undefined` 
**Sample value:** See [actions](./actions.md)

Actions that can be performed on each record on the list or in bulk mode.

### bulk_actions
**Required:** No
**Default:** `undefined` 
**Sample value:** See [actions](./actions.md)

Actions to be perform in bulk (all selected records)

### conditionalFormat
**Required:** No
**Default:** `undefined` 
**Sample value:** `{success: [{operator: 'empty', property_path: 'errors'}]}`

A POJO having css class names as keys of lists of
[conditions](./widgets.md#display_condition-options). Each condition statement
can also have a `priority` property that will determine in which order
conditions are evaluated. Once a condition is matched, that value is returned
and evaluation stops.

### supervisor

**Required:** No
**Default:** `undined` 
**Sample value:** `this`

When a single supervisor is set for one or several `change-list`'s, changes
resulting from any of the "supervised" `change-list` [actions](./actions.md)
will automatically update the relevant "supervised" `change-list`.

**Advanced use:** However the supervisor will not be triggered if the response
object contains `_trigger_supervisor: false`

### allowEdit

**Required:** No
**Default:** `true`
**Sample value:** `false`

When `false` the default icon for the link pointing to the details route
(`changeFormRoute`) will be an eye.
When `true` the default icon  for the link pointing to the details route
(`changeFormRoute`) will be a pencil.

### allowDelete

**Required:** No
**Default:** `true`
**Sample value:** `false`

When `true` a garbage-can icon will be visible on each row, allowing the user to
delete that line.
If `bulkActions` are also available, there will be a `Delete` `bulkAction` available

### allowBulk

**Required:** No
**Default:** `true`
**Sample value:** `false`

When `true` a dropdown will be available in order to perform a specific action
on several records at the same time; a checkbox will also be available on each
line in order to select which records to perform the action on.
