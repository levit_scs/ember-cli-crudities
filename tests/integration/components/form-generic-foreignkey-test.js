import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('form-generic-foreignkey', 'Integration | Component | form generic foreignkey', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{form-generic-foreignkey}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#form-generic-foreignkey}}
      template block text
    {{/form-generic-foreignkey}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
