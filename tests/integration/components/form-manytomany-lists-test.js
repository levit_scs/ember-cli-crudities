import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('form-manytomany-lists', 'Integration | Component | form manytomany lists', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  const model = new Ember.Object({
    related: new Ember.A()
  });
  const extra = {
    property_path: 'related'
  };
  this.set('model', model);
  this.set('extra', extra);
  this.render(hbs`{{form-manytomany-lists model=model extra=extra}}`);

  const result = this.$().text().trim().replace(/ +\n+/g, '');
  assert.ok(result.match(/Available/));
  assert.ok(result.match(/Selected/));
  assert.ok(result.match(/Select all/));
  assert.ok(result.match(/Select none/));
});
