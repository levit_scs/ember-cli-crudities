import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('form-json-fieldset', 'Integration | Component | form json fieldset', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{form-json-fieldset}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#form-json-fieldset}}
      template block text
    {{/form-json-fieldset}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
