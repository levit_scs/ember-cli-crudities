import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

moduleForComponent('form-foreignkey', 'Integration | Component | form foreignkey', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });


  const model = new Ember.Object({
    related: new Ember.A()
  });
  const extra = {
    property_path: 'related'
  };
  this.set('model', model);
  this.set('extra', extra);
  this.render(hbs`{{form-foreignkey model=model extra=extra}}`);

  assert.equal(this.$().text().trim(), '---');
});
