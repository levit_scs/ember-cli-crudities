import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('cf-conditional-formatting-block', 'Integration | Component | cf conditional formatting block', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{cf-conditional-formatting-block}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#cf-conditional-formatting-block}}
      template block text
    {{/cf-conditional-formatting-block}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
