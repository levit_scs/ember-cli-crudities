import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('cl-bulk-actions', 'Integration | Component | cl bulk actions', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{cl-bulk-actions}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#cl-bulk-actions}}
      template block text
    {{/cl-bulk-actions}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
