import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

moduleForComponent('app-dashboard', 'Integration | Component | app dashboard', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  const model = new Ember.A();
  this.set('model', model);
  this.render(hbs`{{app-dashboard model=model}}`);

  assert.equal(this.$().text().trim(), 'Nothing to see here');
});
