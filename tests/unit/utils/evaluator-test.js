import evaluator from 'dummy/utils/evaluator';
import { module, test } from 'qunit';

module('Unit | Utility | evaluator');

// Replace this with your real tests.
test('it works', function(assert) {
  const result = evaluator();
  assert.ok(result);
});
