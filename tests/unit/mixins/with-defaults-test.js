import EmberObject from '@ember/object';
import WithDefaultsMixin from 'ember-cli-crudities/mixins/with-defaults';
import { module, test } from 'qunit';

module('Unit | Mixin | with defaults');

// Replace this with your real tests.
test('it works', function(assert) {
  let WithDefaultsObject = EmberObject.extend(WithDefaultsMixin);
  let subject = WithDefaultsObject.create();
  assert.ok(subject);
});
