import EmberObject from '@ember/object';
import WithConditionMixin from 'ember-cli-crudities/mixins/with-condition';
import { module, test } from 'qunit';

module('Unit | Mixin | with condition');

// Replace this with your real tests.
test('it works', function(assert) {
  let WithConditionObject = EmberObject.extend(WithConditionMixin);
  let subject = WithConditionObject.create();
  assert.ok(subject);
});
