import Ember from 'ember';
import WithValidationMixin from 'ember-cli-crudities/mixins/with-validation';
import { module, test } from 'qunit';

module('Unit | Mixin | with validation');

// Replace this with your real tests.
test('it works', function(assert) {
  let WithValidationObject = Ember.Object.extend(WithValidationMixin);
  let subject = WithValidationObject.create();
  assert.ok(subject);
});
