import Ember from 'ember';
import FilterableActionsMixin from 'ember-cli-crudities/mixins/filterable-actions';
import { module, test } from 'qunit';

module('Unit | Mixin | filterable actions');

// Replace this with your real tests.
test('it works', function(assert) {
  let FilterableActionsObject = Ember.Object.extend(FilterableActionsMixin);
  let subject = FilterableActionsObject.create();
  assert.ok(subject);
});
