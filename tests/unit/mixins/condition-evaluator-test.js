import Ember from 'ember';
import ConditionEvaluatorMixin from 'ember-cli-crudities/mixins/condition-evaluator';
import { module, test } from 'qunit';

module('Unit | Mixin | condition evaluator');

// Replace this with your real tests.
test('it works', function(assert) {
  const ConditionEvaluatorObject = Ember.Object.extend(ConditionEvaluatorMixin);
  const subject = ConditionEvaluatorObject.create();
  assert.ok(subject);
});
