import EmberObject from '@ember/object';
import WithHandleMessageMixin from 'ember-cli-crudities/mixins/with-handle-message';
import { module, test } from 'qunit';

module('Unit | Mixin | with handle message');

// Replace this with your real tests.
test('it works', function(assert) {
  let WithHandleMessageObject = EmberObject.extend(WithHandleMessageMixin);
  let subject = WithHandleMessageObject.create();
  assert.ok(subject);
});
