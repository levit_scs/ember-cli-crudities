import Ember from 'ember';
import ChangeRouteMixin from 'ember-cli-crudities/mixins/change-route';
import { module, test } from 'qunit';

module('Unit | Mixin | change route');

// Replace this with your real tests.
test('it works', function(assert) {
  const ChangeRouteObject = Ember.Object.extend(ChangeRouteMixin);
  const subject = ChangeRouteObject.create();
  assert.ok(subject);
});
