
import { evaluateCondition } from 'dummy/helpers/evaluate-condition';
import { module, test } from 'qunit';

module('Unit | Helper | evaluate condition');

// Replace this with your real tests.
test('it works', function(assert) {
  const result = evaluateCondition([42]);
  assert.ok(result);
});

