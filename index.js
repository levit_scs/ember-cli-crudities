'use strict';

module.exports = {
  name: 'ember-cli-crudities',
  options: {
    babel: {
      plugins: ['transform-object-rest-spread']
    }
  }
};
