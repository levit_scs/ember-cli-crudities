import Ember from 'ember';
import layout from '../templates/components/cf-conditional-formatting-block';

export default Ember.Component.extend({
  layout,
  tagName: '',
});
