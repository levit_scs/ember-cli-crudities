import FormImageComponent from './form-image';
import layout from '../templates/components/form-image-url';

export default FormImageComponent.extend({
  layout,
  type: 'url',
});
