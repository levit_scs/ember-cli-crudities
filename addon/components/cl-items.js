import Ember from 'ember';
import layout from '../templates/components/cl-items';

export default Ember.Component.extend({
  layout,
  tagName: '',
});
