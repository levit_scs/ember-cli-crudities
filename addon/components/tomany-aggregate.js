import Ember from 'ember';
import aggregate from '../utils/records-aggregates';
import layout from '../templates/components/tomany-aggregate';

export default Ember.Component.extend({
  layout,
  classNames: ['aggregate', ],

  observed: [],
  values: {},

  doUpdate() {
    const records = this.get('records');
    const type = this.get('type');
    const propertyPath = this.get('propertyPath');
    const groupBy = this.get('groupBy');
    if (records && type && propertyPath) {
      const values = aggregate(
        records,
        type,
        propertyPath,
        groupBy
      );
      this.set('values', values);
    }
  },

  didInsertElement() {
    this._super();
    const propertyPath = this.get('propertyPath');
    this.set('observed', `records.@each.${propertyPath}`);
    Ember.addObserver(this, `records.@each.${propertyPath}`, this, 'doUpdate');
  },

  willDestroy() {
    this._super();
    Ember.removeObserver(this, this.get('observed'), this, 'doUpdate');
  },

  onInit: Ember.on('init', function() {
    this.doUpdate();
  }),
});
