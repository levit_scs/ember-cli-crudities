import Ember from 'ember';
import layout from '../templates/components/form-generic-foreignkey';
import WithCustomActionsMixin from 'ember-cli-crudities/mixins/with-custom-actions';

export default Ember.Component.extend(WithCustomActionsMixin, {
  layout,
  type: 'foreignkey',

  displayValue: null,
  linkApp: null,
  linkModel: null,
  allowedActions: [],

  contentType: Ember.computed('value', 'value.content_type', function() {
    if (Ember.isEmpty(this.get('value'))) {
      return null;
    }
    const ct_id = this.get('value.content_type');
    return new Ember.RSVP.Promise((resolve, reject) => {
      this.get('store').findRecord('contenttypes/contenttype', ct_id).then((rv) => {
        resolve(rv);
      }, reject);
    });
  }),

  _do_get_related_model(ct, resolve, reject) {
    this.get('modelLoader').ensure_model(ct.get('app_label'), ct.get('model')).then((meta) => {
      if (!this.get('isDestroyed') && !this.get('isDestroying')) {
        const actions = meta.meta.get('custom_actions');
        const allowed = this.get('extra.allowedActions');
        if (Ember.isEmpty(actions)) {
          this.set('allowedActions', []);
        } else if (Ember.isEmpty(allowed) || allowed === undefined) {
          this.set('allowedActions', []);
        } else {
          const allowedActions = actions.filter((action) => {
            return allowed.some((item) => {
              if (action.slug === undefined) {
                return false;
              }
              const r = new RegExp(item);
              return action.slug.match(r);
            });
          });
          this.set('allowedActions', allowedActions);
        }
        this.set('linkApp', ct.get('app_label'));
        this.set('linkModel', ct.get('model'));
      }
      resolve(ct);
    }, reject);
  },

  relatedModel: Ember.computed('contentType', function() {
    return new Ember.RSVP.Promise((resolve, reject) => {
      const content_type = this.get('contentType');
      if (content_type.hasOwnProperty('isFulfilled') || content_type.then) {
        content_type.then((ct) => {
          this._do_get_related_model(ct, resolve, reject);
        }, reject);
      } else {
        this._do_get_related_model(content_type, resolve, reject);
      }
    });
  }),

  onValueChange: Ember.observer('value', 'relatedModel', 'value.id', function() {
    this.setDisplayVal();
  }),

  valueId: Ember.computed('value', 'value.id', function() {
    if (Ember.isEmpty(this.get('value'))) {
      return null;
    }
    return this.get('value.id');
  }),

  changeRelatedModel(value) {
    if (value === 'contentType') {
      return;
    }
    this.set(`model.${this.get('property')}`, {
      content_type: Ember.get(value, 'id'),
      id: this.get('value.id')
    });
  },

  changeRelatedId(value) {
    if (value === 'displayValue') {
      return;
    }
    this.set(`model.${this.get('property')}`, {
      content_type: this.get('value.content_type'),
      id: Ember.get(value, 'id')
    });
  },

  setDisplayVal() {
    const valueId = this.get('valueId');
    if (Ember.isEmpty(valueId)) {
      return null;
    }
    const ct_prom = this.get('relatedModel');
    if (Ember.isEmpty(ct_prom)) {
      return null;
    }
    ct_prom.then((ct) => {
      this.get('store').findRecord(`${ct.get('app_label')}/${ct.get('model')}`, valueId).then((rec) => {
        if (!this.get('isDestroyed') && !this.get('isDestryoing')) {
          this.set('displayValue', rec);
        }
      });
    });
  },

  extraForContentType: Ember.computed('extra', function() {
    const extra = this.get('extra');
    return {
      ...extra,
      related_model: 'contenttypes/contenttype'
    };
  }),

  _do_set_extra_with_content_type(ct) {
    const extra = this.get('extra');
    const rv = {
      ...extra,
      related_model: `${ct.get('app_label')}/${ct.get('model')}`
    };
    if (!this.get('isDestroyed') && !this.get('isDestroiyng')) {
      this.set('extraWithRelated', rv);
    }
  },

  setExtraWithContentType() {
    const content_type = this.get('contentType');
    if (Ember.isEmpty(content_type)) {
      return;
    }
    if (content_type.hasOwnProperty('isFulfilled') || content_type.then) {
      content_type.then((ct) => {
        this._do_set_extra_with_content_type(ct);
      });
    } else {
      this._do_set_extra_with_content_type(content_type);
    }
  },

  onChangeContentType: Ember.observer('extra', 'contentType', function() {
    this.setExtraWithContentType();
  }),

  init() {
    this._super();
    this.setDisplayVal();
    this.setExtraWithContentType();
  }
});
