import Ember from 'ember';
import FormInput from './form-input';

export default FormInput.extend({
  type: 'checkbox',
  _checkbox_class: 'checkbox-primary',
  checkboxClass: Ember.computed.or('extra.checkboxClass', '_checkbox_class'),
});
