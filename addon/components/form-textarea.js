import Ember from 'ember';
import layout from '../templates/components/form-textarea';
import BoundValueMixin from'../mixins/boundvalue';

export default Ember.Component.extend(BoundValueMixin, {
  layout,
  tagName: undefined,
  type: 'textarea',

  _rows: 4,
  rows: Ember.computed.or('extra.rows', '_rows'),

  actions: {
    validate(parent) {
      parent.validate();
    },
  }
});
