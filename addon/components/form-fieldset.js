import Ember from 'ember';
import BaseCollection from './base-collection';
import layout from '../templates/components/form-fieldset';

export default BaseCollection.extend({
  layout,

  is_ready: true,

  tagName: 'fieldset',
  classNameBindings: ['class', ],
  fields: Ember.A(),
  label: Ember.computed.or('field.title', 'field.label'),
  innerClass: Ember.computed('field.innerClass', function() {
    const innerClass = this.get('field.innerClass');
    if (!Ember.isEmpty(innerClass)) {
      return innerClass;
    }
    return 'col-xs-12 stretch-row';
  }),

  horiClass: Ember.computed.alias('field.horiClass'),
  inputClass: Ember.computed.alias('field.inputClass'),

  onChange() {
    // placeholder function
  },
});
