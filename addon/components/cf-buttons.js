import Ember from 'ember';
import layout from '../templates/components/cf-buttons';

export default Ember.Component.extend({
  layout,
  tagName: '',
  showSave: true,
  saveText: 'Save',
  saveClass: 'fa-floppy-o',
  saveAndContinueText: 'Save and continue editing',
  saveAndContinueClasses: ['fa-floppy-o', 'fa-pencil'],
  cancelText: 'Cancel',
  cancelClass: 'fa-reply',
  deleteText: 'Delete',
  deleteClass: 'fa-trash',
});
