import Ember from 'ember';
import layout from '../templates/components/base-widget';
import BoundValueMixin from '../mixins/boundvalue';
import Validator from '../utils/validation';

export default Ember.Component.extend(BoundValueMixin, {
  layout,

  classNames: ['form-group'],
  classNameBindings: ['field.translated:translated', 'hasError', 'required', 'readonly', 'typeClass'],

  label: null,
  type: 'text',
  horiClass: 'col-sm-3',
  inputClass: 'col-sm-9',
  errors: [],
  validations: {},
  placeholder: Ember.computed.or('field.placeholder', 'field.extra.placeholder'),

  typeClass: Ember.computed('type', function() {
    return `form-${this.get('type')}`;
  }),

  isInput: Ember.computed('type', function() {
    const type = this.get('type');
    if (-1 !== ['text', 'tel', 'email', 'number', 'date', 'time', 'datetime', 'color', 'month',
      'week', 'url', 'password'].indexOf(type)) {
      return true;
    }
    return false;
  }),

  isDateTime: Ember.computed('type', function() {
    const type = this.get('type');
    if (-1 !== ['date', 'time', 'datetime', 'month', 'week'].indexOf(type)) {
      return true;
    }
    return false;
  }),

  momentFormat: Ember.computed('type', function() {
    const type = this.get('type');
    if (type === 'date') {
      return 'ddd YYYY-MM-DD';
    }
    if (type === 'time') {
      return 'kk:mm:ss';
    }
    return 'ddd YYYY-MM-DD HH:mm:ss zz';
  }),

  hasLabel: Ember.computed('label', 'formLayout', 'type', function() {
    const formLayout = this.get('formLayout');
    const label = this.get('label');
    const type = this.get('type');
    return !Ember.isEmpty(label) && formLayout === 'horizontal' && type !== 'checkbox';
  }),

  bsInputClass: Ember.computed('inputClass', 'hasLabel', function() {
    const hasLabel = this.get('hasLabel');
    if (!hasLabel) {
      return 'col-xs-12';
    }
    return this.get('inputClass');
  }),

  buildValidator() {
    const validations = this.get('validations');
    const validator = Validator.create({validations});
    if (!this.get('isDestroyed') && !this.get('isDestroying')) {
      if (this.get('required') && !Ember.isEmpty(validations)) {
        Ember.set(validations, 'presence', true);
      }
      if (this.get('model.form_errors') === undefined) {
        this.set('model.form_errors', {});
      }
      this.set('validator', validator);
    }
    return validator;
  },

  status: Ember.computed('model.isError', 'model.errors.@each.length', 'errors',
    'model.errors', 'model.form_errors', 'model.form_errors.@each.length',
    'model.form_errors.@each', function() {
      if (this.get(`model.errors.${this.get('property')}.length`)) {
        return 'error';
      }
      if (this.get(`model.form_errors.${this.get('property')}.length`)) {
        return 'error';
      }
      if (this.get('errors.length')) {
        return 'error';
      }
      return 'success';
    }),

  hasError: Ember.computed.equal('status', 'error'),

  validate() {
    let validator = this.get('validator');
    const value = this.get(`model.${this.get('property')}`);

    if (validator === undefined) {
      validator = this.buildValidator();
    }
    if (validator !== undefined) {
      validator.validate(value);

      this.set('errors', validator.get('errors'));
      this.set(`model.form_errors.${this.get('property')}`, validator.get('errors'));
      return validator.get('isValid');
    }
    return true;
  },

  actions: {
    // for sub-classes
    validate(parent) {
      parent.validate();
    }
  }
});
