import Ember from 'ember';
import layout from '../templates/components/form-select';
import BoundValueMixin from'../mixins/boundvalue';

export default Ember.Component.extend(BoundValueMixin, {
  layout,
  tagName: undefined,
  type: 'select',
  _placeholder: '---',
  _other: {value: '__other__', label: 'Other'},
  _is_bool: false,

  placeHolder: Ember.computed.or('field.placeholder', 'placeholder', '_placeholder'),
  isBool: Ember.computed.or('extra.isBool', '_is_bool'),

  choices: Ember.computed('extra.choices', 'extra.choices[]', 'extra.allowOther', function() {
    const choices = new Ember.A(Ember.copy(this.get('extra.choices')));
    if (this.get('extra.allowOther')) {
      choices.pushObject(this.get('_other'));
    }
    return choices;
  }),

  selectedItem: Ember.computed('value', 'choices', 'choices[]', {
    get() {
      let value = this.get('value');
      const isBool = this.get('isBool');
      if (isBool) {
        if (value === true) {
          value = 1;
        } else if (value === false) {
          value = 0;
        } else {
          value = null;
        }
      }
      const filtered = this.get('choices').filter((item) => {
        return item.value === null && Ember.isEmpty(value) || item.value === value;
      });
      if (filtered.length > 0) {
        return filtered[0];
      }
      if (this.get('extra.allowOther') && this.get('value')) {
        return this.get('_other');
      }
      return null;
    },
    set(key, value) {
      const other = this.get('_other');
      if (value !== null) {
        if (value.value !== other.value) {
          this.set('value', value.value);
        } else {
          this.set('value', '');
        }
      } else {
        this.set('value', null);
      }
      return value;
    }
  }),

  valueStr: Ember.computed('value', 'choices', 'choices[]', function() {
    const value = this.get('value');
    const choice = this.get('choices').filter((item) => {
      return item.value === value;
    });
    if (choice.length > 0) {
      return choice[0].label;
    }
    return value;
  }),

  actions: {
    change(selectedItem) {
      this.set('selectedItem', selectedItem);
    },

    updateOtherVal(val) {
      const other = this.get('_other');
      if (this.get('selectedItem.value') === other.value) {
        this.set('value', val);
      }
    },

    validate(parent) {
      parent.validate();
    }
  }
});
