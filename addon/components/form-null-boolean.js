import Ember from 'ember';
import Select from './form-select';
import layout from '../templates/components/form-select';

export default Select.extend({
  layout,

  type: 'null-boolean',

  _placeholder: false,
  _is_bool: true,

  allowNull: true,

  choices: Ember.computed('allowNull', 'extra.preventNull', 'extra.choices', function() {
    const extraChoices = this.get('extra.choices');
    if (extraChoices) {
      return extraChoices;
    }
    const allowNull = this.get('allowNull') && !this.get('extra.preventNull');
    const rv = [];
    if (allowNull) {
      rv.push({
        label: 'None',
        value: null,
      });
    }
    return rv.concat([{
      label: 'Yes',
      value: 1,
    }, {
      label: 'No',
      value: 0
    }]);
  }),
});
