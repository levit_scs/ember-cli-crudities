import Ember from 'ember';
import Dashboard from './app-dashboard';
import layout from '../templates/components/app-menu';

export default Dashboard.extend({
  layout,
  tagName: 'ul',

  routing: Ember.inject.service('-routing'),
  currentApp: null,
  currentModel: null,

  routeChange: Ember.observer('routing.currentState', function() {
    try {
      let router = this.get('routing.currentState.emberRouter.router');
      if (Ember.isEmpty(router)) {
        const App = Ember.getOwner(this);
        router = App.lookup('router:main').get('currentState.routerJs');
      }
      const state = Ember.get(router, 'currentHandlerInfos');
      const app = Ember.get(state[2].params, 'app_name');
      if (app) {
        this.set('currentApp', app);
      } else {
        this.set('currentApp', null);
      }
      const model = Ember.get(state[2].params, 'model_name');
      if (model) {
        this.set('currentModel', model);
      } else {
        this.set('currentModel', null);
      }
    } catch(e) {
      console.warn(e);
      this.set('currentApp', null);
      this.set('currentModel', null);
    }
  }),

  init() {
    this._super();
    Ember.run.later(this, this.routeChange, 150);
  },

  actions: {
    closeAndTransition(route, app_name, model_name, id) {
      if (this.get('parent')) {
        this.set('parent.open', false);
      }
      if (app_name) {
        if (model_name) {
          if (!Ember.isEmpty(id)) {
            this.get('routing').transitionTo(route, [app_name, model_name, id]);
          } else {
            this.get('routing').transitionTo(route, [app_name, model_name]);
          }
        } else  {
          this.get('routing').transitionTo(route, [app_name]);
        }
      } else {
        this.get('routing').transitionTo(route);
      }
    }
  }
});
