import Ember from 'ember';
import layout from '../templates/components/cl-headers';

export default Ember.Component.extend({
  layout,
  tagName: 'tr'
});
