import Ember from 'ember';
import layout from '../templates/components/app-dashboard';
import ConfigurableLoaderMixin from '../mixins/configurable-loader';

export default Ember.Component.extend(ConfigurableLoaderMixin, {
  layout,
  modelLoader: Ember.inject.service(),
  model: new Ember.A(),
  willRender() {
    if (this.attrs.model !== undefined) {
      return;
    }
    const modelLoader = this.get('modelLoader');
    modelLoader.fetch_app_meta().then((metadata) => {
      const rv = metadata.applications;
      this.set('model', rv);
    });
  },
});
