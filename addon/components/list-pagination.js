import Ember from 'ember';
import layout from '../templates/components/list-pagination';

export default Ember.Component.extend({
  layout,
  classNames: ['pagination', 'pull-right'],
  hasPages: Ember.computed('meta', function() {
    const meta = this.get('meta');
    if (!meta) {
      return false;
    }
    const currentPage = this.get('currentPage');
    if (meta.next && meta.next > currentPage) {
      return true;
    }
    return meta.previous;
  }),
  pageCount: Ember.computed('meta', function() {
    const next = this.get('meta.next');
    const currentPage = this.get('currentPage');
    if (!next || next < currentPage) {
      const previous = this.get('meta.previous');
      return parseInt(previous) + 1;
    }
    const records = parseInt(this.get('model.length'));
    const count = parseInt(this.get('meta.count'));
    return Math.ceil(count / records);
  }),

  begining: Ember.computed('pageCount', 'currentPage', function() {
    const currentPage = this.get('currentPage');
    const pageCount = this.get('pageCount');
    if (pageCount < 15) {
      return pageCount;
    }
    if (currentPage < 10) {
      return Math.max(currentPage + 2, 5);
    }
    return 5;
  }),

  middle: Ember.computed('pageCount', 'currentPage', function() {
    const currentPage = this.get('currentPage');
    const pageCount = this.get('pageCount');
    if (pageCount < 15 || currentPage < 10 || currentPage + 10 >= pageCount) {
      return false;
    }
    return {from: currentPage - 2, to: currentPage + 3};
  }),

  end: Ember.computed('pageCount', 'currentPage', function() {
    const currentPage = this.get('currentPage');
    const pageCount = this.get('pageCount');
    if (pageCount < 15) {
      return false;
    }
    if (currentPage >= pageCount - 10) {
      return Math.min(currentPage - 2, pageCount - 4);
    }
    return pageCount - 4;
  }),
});
