import Ember from 'ember';
import layout from '../templates/components/form-json-fieldset';

export default Ember.Component.extend({
  layout,
  dynField: Ember.computed('model', 'model.isFulfilled', 'field', 'field.extra', 'field.extra.data', 'field.extra.data.@each', 'field.fields', 'field.fields.@each', function() {
    const field = new Ember.Object(this.get('field'));
    if (Ember.isEmpty(field.get('fields'))) {
      const fields = this.get(`model.${field.get('extra.data')}`);
      field.set('fields', fields);
    }
    field.set('name', undefined);
    return field;
  }),
});
