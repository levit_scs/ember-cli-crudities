import Ember from 'ember';
import ToManyBase from './tomany-base';
import layout from '../templates/components/form-tomany-slide';

const {
  computed
} = Ember;

export default ToManyBase.extend({
  layout,

  currentIndex: 0,
  indexPlusOne: computed('currentIndex', 'related.length', 'related', function() {
    const related_length = this.get('related.length');
    if (related_length) {
      const currentIndex = this.get('currentIndex');
      return currentIndex + 1;
    }
    return 0;
  }),
  record: computed('related.length', 'currentIndex', 'related', function() {
    const index = this.get('currentIndex');
    const related = this.get('related');
    if (related && index < related.get('length')) {
      return related.objectAt(index);
    }
    return null;
  }),

  direction: 'toLeft',

  actions: {
    previous() {
      this.set('direction', 'toRight');
      const index = this.get('currentIndex');
      if (index > 0) {
        this.set('currentIndex', parseInt(index) - 1);
      } else {
        const length = this.get('related.length');
        if (length > 0) {
          this.set('currentIndex', length - 1);
        }
      }
    },
    next() {
      this.set('direction', 'toLeft');
      const index = parseInt(this.get('currentIndex'));
      const length = this.get('related.length');
      if (index < length - 1) {
        this.set('currentIndex', index + 1);
      } else if (length > 0) {
        this.set('currentIndex', 0);
      }
    },
    addRelated() {
      this._super();
      this.set('currentIndex', parseInt(this.get('related.length') - 1));
    },
    deleteRelated(record) {
      this.set('currentIndex', 0);
      this._super(record);
    }
  },
});
