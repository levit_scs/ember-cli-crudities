import Ember from 'ember';
import layout from '../templates/components/list-filters';

export default Ember.Component.extend({
  layout,
  tagName: null,
  doFilterValueChanged(param, value) {
    this.attrs.parent.filterValueChanged(param, value);
  },
  didReceiveAttrs() {
    if (this.attrs.parent !== undefined) {
      const fields = ['search_enabled', 'searchValue', 'filter_fields', 'filterFields',
        'filterValues', '_hidden_filters', '_filter_values'];
      fields.forEach((field) => {
        if (this.attrs[field] === undefined) {
          this.set(field, Ember.computed.alias(`parent.${field}`));
        }
      });
    }
  }
});
