import Ember from 'ember';
import layout from '../templates/components/cl-bulk-actions';

export default Ember.Component.extend({
  layout,
  tagName: '',
});
