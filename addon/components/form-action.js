import Ember from 'ember';
import layout from '../templates/components/form-action';

export default Ember.Component.extend({
  layout,
  status: 'success',
  hasError: false,
  validate() {
    return Ember.RSVP.resolve(true);
  }
});

