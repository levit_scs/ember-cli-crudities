import Ember from 'ember';
import layout from '../templates/components/custom-action';
import WithConditionMixin from '../mixins/with-condition';

export default Ember.Component.extend(WithConditionMixin, {
  layout,
  _disabled: false,
  toWatch: [['act.disable_condition', '_disabled']],

  isComponent: Ember.computed('act.component', function() {
    const component = this.get('act.component');
    return component !== undefined;
  }),
  isMethod: Ember.computed('act.type', function() {
    const type = this.get('act.type');
    return type === 'modelMethod' || type === 'closureMethod';
  }),

});
