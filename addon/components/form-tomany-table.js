import Ember from 'ember';
import ToManyStack from './form-tomany-stack';
import layout from '../templates/components/form-tomany-table';

export default ToManyStack.extend({
  layout,
  classNames: ['tomany-table'],

  actionColumns: Ember.computed('field.extra.customActions', 'field.extra.customActions.length', 'field.extra.allowOpen', 'field.extra.forceActions', function() {
    let forceActions = this.get('field.extra.forceActions');
    if (Ember.isEmpty(forceActions)) {
      forceActions = [];
    }
    const actionsToShow = this.get('custom_actions').filter((act) => {
      if (!act.hide && !act.hideInList && !act.hideInToMany) {
        return true;
      }
      return forceActions.indexOf(act.slug) > -1;
    });
    const rv = parseInt(actionsToShow.length ? actionsToShow.length : 0) +
      parseInt(this.get('field.extra.allowOpen') ? 1 : 0);
    return rv;
  }),

  column_count: Ember.computed('field.fields.length', 'actionColumns', function() {
    const readonly = this.get('readonly');
    const rv = parseInt(this.get('field.fields.length')) +
      parseInt(readonly !== true ? 1 : 0) +
      parseInt(!readonly && this.get('field.extra.sortableBy') ? 1 : 0) +
      parseInt(this.get('actionColumns'));
    return rv;
  }),
});
