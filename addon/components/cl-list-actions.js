import Ember from 'ember';
import layout from '../templates/components/cl-list-actions';

export default Ember.Component.extend({
  layout,
  tagName: '',
});
