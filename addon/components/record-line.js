import Ember from 'ember';
import layout from '../templates/components/record-line';
import SortableItem from 'ember-sortable/mixins/sortable-item';
import withCustomActionsMixin from '../mixins/with-custom-actions';
import conditionalFormat from '../utils/conditional-format';

export default Ember.Component.extend(SortableItem, withCustomActionsMixin, {
  layout,
  tagName: 'tr',
  classNameBindings: ['state', 'conditionalFormatting'],
  timer: undefined,
  handle: '#none',
  observed: [],

  state: '',
  computeFormatting() {
    const model = this.get('model');
    const conditions = this.get('conditionalFormat');
    this.set('conditionalFormatting', conditionalFormat(model, conditions));
  },

  willDestroy() {
    this._super();
    this.get('observed').forEach((item) => {
      this.removeObserver(`model.${item.split('-')[1]}`, this.computeFormatting.bind(this));
    });
  },

  didReceiveAttrs() {
    this.computeFormatting();
    const conditions = this.get('conditionalFormat');
    const observed = this.get('observed');
    let prop;
    let i;
    for (prop in conditions) {
      if (conditions.hasOwnProperty(prop)) {
        for (i=0; i<conditions[prop].length; i++) {
          if (observed.indexOf(`${this.get('model.id')}-${conditions[prop][i].property_path}`) === -1) {
            this.addObserver(`model.${conditions[prop][i].property_path}`, this.computeFormatting.bind(this));
            observed.push(`${this.get('model.id')}-${conditions[prop][i].property_path}`);
          }
        }
      }
    }

    if (this.attrs.parent !== undefined) {
      const fields = ['display_fields', 'innerClass', 'changeFormRoute', 'app_name', 'label',
        'orderable', 'custom_actions'];
      fields.forEach((field) => {
        if (this.attrs[field] === undefined) {
          this.set(field, this.get(`parent.${field}`));
        }
      });
    }
  },

  actions: {
    addNew() {

    },
    placeHolder() {

    },
    save(/* model, field */) {
      Ember.run.debounce(this, () => {
        this.get('model').save().then(() => {
          if (this.timer !== undefined) {
            Ember.run.cancel(this.timer);
          }
          this.set('state', 'success');
          this.timer = Ember.run.later(this, () => {
            this.set('state', '');
          }, 750);
        }).catch((err) => {
          /* eslint-disable no-console */
          console.error(err);
          /* eslint-enable no-console */
          if (this.timer !== undefined) {
            Ember.run.cancel(this.timer);
          }
          this.set('state', 'danger');
          this.timer = Ember.run.later(this, () => {
            if (!this.get('isDestroyed')) {
              this.set('state', '');
            }
          }, 750);
        });
      }, 250);
    },
  }
});
