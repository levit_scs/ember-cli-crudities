import Ember from 'ember';
import ConfigurableLoaderMixin from '../mixins/configurable-loader';

export default Ember.Component.extend(ConfigurableLoaderMixin, {
  modelLoader: Ember.inject.service(),

  fetch_meta: true,
  is_ready: false,

  custom_actions: [],
  bulk_actions: [],
  ordering_fields: [],
  filter_fields: [],
  conditional_format: {},

  __horiClass: 'col-sm-3',
  _horiClass: Ember.computed.or('horiClass', '__horiClass'),
  __inputClass: 'col-sm-9',
  _inputClass: Ember.computed.or('inputClass', '__inputClass'),

  fieldsChanged: function() {
    const rv = Ember.A();
    const innerHoriClass = this.get('_horiClass');
    const innerInputClass = this.get('_inputClass');
    const fields = this.get('field.fields');
    if (!Ember.isEmpty(fields)) {
      try {
        fields.forEach((field) => {
          if (Ember.isEmpty(field.horiClass)) {
            field.horiClass = innerHoriClass;
          }
          if (Ember.isEmpty(field.inputClass)) {
            field.inputClass = innerInputClass;
          }
          if (!Ember.isEmpty(field.target_model) && field.targetModel === undefined) {
            const target_model = this.get('store').findAll(field.model);
            field.targetModel = target_model;
          }
          rv.pushObject(new Ember.Object(field));
        });
      } catch(e) {
        console.warn(e);
      }
    }
    this.set('fields', rv);
    if (this.get('model.content') !== undefined) {
      this.get('model').addObserver('content.isLoaded', this._update_ready.bind(this));
      this._update_ready();
    } else {
      this.set('is_ready', true);
    }
  },

  _update_ready() {
    const loaded = this.get('model.content.isLoaded');
    if (loaded && !this.get('isDestroyed')) {
      this.set('is_ready', true);
      this.get('model').removeObserver('content.isLoaded', this._update_ready.bind(this));
    }
  },

  willDestroyElement() {
    try {
      this.get('model').removeObserver('content.isLoaded', this._update_ready.bind(this));
    } catch(e) {
      // do nothing;
    }
  },

  save() {
    const do_save = this.get('field.extra.save');
    if (do_save === true) {
      this.get('model').then((model) => {
        model.save();
      });
    }
  },

  delete() {
    const do_save = this.get('field.extra.save');
    if (do_save === true) {
      this.get('model').then((model) => {
        model.destroyRecord();
      });
    }
  },

  _fetchMeta(model) {
    if (this.get('fetch_meta') === true) {
      try {
        const fieldset = this.get('field');
        const modelLoader = this.get('modelLoader');
        const inflector = new Ember.Inflector(Ember.Inflector.defaultRules);

        let model_name = this.get('field.extra.related_model');
        if (model_name === undefined) {
          model_name = model.get('content._internalModel.type.modelName');
        }
        if (model_name === undefined) {
          model_name = model.get('content.type.modelName');
        }
        if (model_name === undefined) {
          model_name = model.get('_internalModel.type.modelName');
        }
        if (model_name === undefined) {
          model_name = model.get('type.modelName');
        }
        if (model_name === undefined) {
          throw 'Undefined model_name';
        }
        const plural_name = model_name.split('/');
        plural_name[1] = inflector.pluralize(plural_name[1]);

        modelLoader.get_model_meta(plural_name[0], plural_name[1]).then((meta) => {
          const merged = modelLoader.merge_fields_fieldset(meta.fields, fieldset);
          if (!this.get('isDestroyed') && !Ember.isEmpty(fieldset.fields)) {
            Ember.set(fieldset, 'fields', merged.fields);
            const def = {};
            meta.fields.forEach((field) => {
              const field_default = Ember.get(field, 'extra.default');
              if (field_default) {
                def[Ember.get(field, 'name')] = field_default;
              }
            });
            this.set('_defaults', def);
            this.set('field', fieldset);
            this.set('fetch_meta', false);

            const items = ['custom_actions', 'bulk_actions', 'ordering_fields', 'filter_fields',
              'conditional_formatting'];
            items.forEach((item) => {
              this.set(item, meta[item]);
            });

            this.fieldsChanged();
          } else if (Ember.isEmpty(fieldset.fields)) {
            console.warn('Empty fieldset for', plural_name);
          }
        }, (err) => {
          console.error('while loading meta', err);
        });
      } catch (e) {
        console.error('undefined model_name', e);
      }
    }  else {
      this.fieldsChanged();
    }
  },

  didReceiveAttrs() {
    const model = this.get('model');
    if (model !== undefined && model !== null && model.hasOwnProperty('isFulfilled')) {
      this.get('model').then((model) => {
        this._fetchMeta(model);
      });
    } else if (model !== undefined) {
      this._fetchMeta(model);
    } else {
      console.error(model, 'is undefined', this.get('field.name'));
    }
  },
});
