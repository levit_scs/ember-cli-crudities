import Ember from 'ember';
import layout from '../templates/components/cf-custom-actions';

export default Ember.Component.extend({
  layout,
  tagName: null,
  actionTagName: 'div',
});
