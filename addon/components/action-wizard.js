import Ember from 'ember';
import layout from '../templates/components/action-wizard';
import WithValidationMixin from '../mixins/with-validation';
import WithHandleMessageMixin from '../mixins/with-custom-actions';
import { task } from 'ember-concurrency';

export default Ember.Component.extend(WithValidationMixin, WithHandleMessageMixin, {
  store: Ember.inject.service(),
  flashMessages: Ember.inject.service(),
  router: Ember.inject.service(),
  layout,
  tetherClass: 'modal-dialog modal-lg',
  tetherAttachment: 'middle center',
  tetherTargetAttachment: 'middle center',
  tetherTarget: 'document.body',
  fieldsets: Ember.computed.alias('params'),

  doRqst: task(function * (url, act, model) {
    yield this.validate(() => {
      this.wizard_action(url, act, model, true).then(() => {
        this.overlayClick();
        if (act.redirect && !model.get('isError')) {
          const router = this.get('router');
          router.transitionTo(act.redirect);
        }
      }, (err) => {
        console.error(err);
        this.get('flashMessages').danger('There was an error while prforming the action');
      });
    }, (err) => {
      console.error(err);
      this.get('flashMessages').danger('Please correct the errors in the form');
    });
  }),

  buttonsDisabled: Ember.computed.alias('doRqst.isRunning'),

  actions: {
    cancel() {
      this.overlayClick();
    },
  }
});
