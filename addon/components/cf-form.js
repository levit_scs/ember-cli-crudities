import Ember from 'ember';
import layout from '../templates/components/cf-form';

export default Ember.Component.extend({
  layout,
  tagName: '',
});
