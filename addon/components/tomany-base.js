import Ember from 'ember';
import BaseCollection from './base-collection';

const {
  computed,
  isEmpty
} = Ember;

export default BaseCollection.extend({
  store: Ember.inject.service(),
  saveMePrefix: '',

  related: computed('model', 'model.length', 'model.@each.isSoftDeleted', 'extra.hideDeleted', function() {
    const value = this.get('model');
    if (!isEmpty(value)) {
      if (!this.get('extra.reload')) {
        const toReload = [];
        value.filter((record) => {
          return record.get('isEmpty');
        }).forEach((record) => {
          toReload.push(record.get('id'));
        });
        if (toReload.length > 0) {
          this.get('store').query(value.get('content.type.modelName'), {ids: toReload});
        }
      }
      if (this.get('extra.hideDeleted')) {
        const rv = value.filter(function(item) {
          return !item.get('isSoftDeleted');
        });
        return rv;
      }
    }
    return value;

  }),

  save() {
    this.get('model').then((model) => {
      model.forEach((record) => {
        if (!record.get('isSoftDeleted')) {
          record.save();
        } else {
          record.destroyRecord();
        }
      });
    });
  },

  delete() {
    this.get('model').then((model) => {
      model.forEach((record) => {
        record.destroyRecord();
      });
    });
  },

  getDefaults() {
    const rv = Ember.copy(this.get('_defaults'));
    if (this.get('field.extra.sortableBy')) {
      rv[this.get('field.extra.sortableBy')] = parseInt(this.get('related.length')) + 1;
    }
    return rv;
  },

  model_name: Ember.computed('model', function() {
    const value = this.get('model');
    if (!isEmpty(value)) {
      return value.get('content.type.modelName');
    }
    return '';
  }),

  related_app: Ember.computed('model_name', function() {
    const name = this.get('model_name');
    if (name === '') {
      return '';
    }
    return name.split('/')[0];
  }),

  related_singular: Ember.computed('model_name', function() {
    let name = this.get('model_name');
    if (name === '') {
      return '';
    }
    name = name.split('/');
    return name[name.length - 1];
  }),

  saveMe() {
    if (this.get('field.name')) {
      const saveMes = this.get('saveMes');
      if (saveMes !== undefined) {
        saveMes.push(`${this.get('saveMePrefix')}${this.get('field.name')}`);
      }
    }
  },

  actions: {
    addRelated() {
      const related = this.get('model');
      let ct = related.get('content.type.modelName');
      if (Ember.isEmpty(ct)) {
        ct = related.get('relationship.belongsToType');
      }
      const record = this.get('store').createRecord(ct, this.getDefaults());
      related.pushObject(record);
      this.saveMe();
    },
    popupAddRelated(model) {
      const related = this.get('model');
      const record = this.get('store').createRecord(model, this.getDefaults());
      related.unshiftObject(record);
      this.addRelated(model, this.get('field'), false, record);
      this.saveMe();
    },
    deleteRelated(record) {
      if (record.get('isNew')) {
        record.rollbackAttributes();
      } else {
        record.set('isSoftDeleted', !record.get('isSoftDeleted'));
      }
      this.saveMe();
    },
  },

  // didInsertElement() {
  //   Ember.run.later(this, function() {
  //     if (!this.get('isDestroyed')) {
  //       this.rerender();
  //     }
  //   }, 750);
  // },
});
