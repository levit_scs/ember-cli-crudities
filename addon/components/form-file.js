import Ember from 'ember';
import layout from '../templates/components/form-file';
import BoundValueMixin from'../mixins/boundvalue';

export default Ember.Component.extend(BoundValueMixin, {
  layout,
  tagName: undefined,
  type: 'file',
  _filename: null,

  fileName: Ember.computed('value', '_filename', function() {
    const value = this.get('value');
    if (value.indexOf('data:') === 0) {
      return this.get('_filename');
    }
    return value;
  }),

  actions: {
    upload(ev) {
      const value = ev.target.files;
      if (value && value[0]) {
        const reader = new FileReader();
        reader.onload = (e) => {
          const data = e.target.result;
          this.set('value', data);
        };
        reader.readAsDataURL(value[0]);
        this.set('_filename', value[0].name);
      }
    },

    clear() {
      this.set('value', null);
    },

    validate(parent) {
      parent.validate();
    },
  }
});
