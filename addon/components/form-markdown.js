import Ember from 'ember';
import TextAreaComponent from './form-textarea';
import layout from '../templates/components/form-markdown';

export default TextAreaComponent.extend({
  layout,
  type: 'markdown',
  classNames: ['markdown', ],

  _hideIcons: ['image', 'fullscreen'],
  _showIcons: ['table', 'code', 'horizontal-rule'],

  hideIcons: Ember.computed.or('extra.hideIcons', '_hideIcons'),
  showIcons: Ember.computed.or('extra.showIcons', '_showIcons'),

  buildSimpleMDEOptions: Ember.computed('hideIcons', 'showIcons', function() {
    return {
      showIcons: this.get('showIcons'),
      hideIcons: this.get('hideIcons'),
    };
  }),
});
