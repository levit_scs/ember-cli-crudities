import Ember from 'ember';
import { task } from 'ember-concurrency';
import Inflector from 'ember-inflector';
import layout from '../templates/components/change-list';
import withCustomActionsMixin from '../mixins/with-custom-actions';
import ConditionEvaluatorMixin from '../mixins/condition-evaluator';
import filterableActionsMixin from '../mixins/filterable-actions';

export default Ember.Component.extend(filterableActionsMixin, withCustomActionsMixin, ConditionEvaluatorMixin, {
  layout,
  classNames: ['row'],
  inflector: Inflector.inflector,
  store: Ember.inject.service(),
  flashMessages: Ember.inject.service(),
  ordering_fields: [],
  filter_fields: [],
  bulk_actions: [],
  list_actions: [],
  search_enabled: false,
  _search_value: null,
  filterValues: new Ember.Object(),
  _filter_values: {},
  _hidden_filters: [],
  sortBy: ['id'],
  sorted: Ember.computed.sort('model', 'sortBy'),
  conditionalFormat: {},
  showCFLegend: false,
  initOnAttrRec: true,

  allowAdd: true,
  allowBulk: true,
  allowDelete: true,
  allowEdit: true,
  showPagination: true,
  displayLabel: true,
  limit: null,

  modalOpen: false,
  modalMessage: undefined,

  orderable: Ember.computed('sortableBy', 'filterValues.ordering', function() {
    const sortableBy = this.get('sortableBy');
    if (!sortableBy) {
      return false;
    }
    const ordering = this.get('filterValues.ordering');
    if (Ember.isEmpty(ordering)) {
      return true;
    }
    return sortableBy === ordering;
  }),

  searchValue: Ember.computed('_search_value', {
    get() {
      return this.get('_search_value');
    },
    set(key, value) {
      this.set('_search_value', value);
      Ember.run.debounce(this, () => {
        const val = this.get('_search_value');
        if (val === value) {
          this.filterModel(val, this.get('_filter_values'), 1);
        }
      }, 300);
      return value;
    }
  }),

  filterFields: Ember.computed('filter_fields', 'filter_fields[]', function() {
    const fields = this.get('fields');
    const filter_fields = this.get('filter_fields');
    const rv = Ember.A();

    filter_fields.forEach((field_name) => {
      let field = {};
      if (typeof field_name === 'object' && !Ember.isEmpty(field_name)) {
        field = {...field_name};
        field_name = field_name.name;
      }
      field_name = field_name.replace(/_id$/, '');
      if (fields && fields !== undefined) {
        const filtered = fields.filterBy('name', field_name);
        if (filtered.length > 0) {
          field = {...filtered[0], ...field};
        }
      }

      Ember.set(field, 'readonly', false);
      Ember.set(field, 'required', false);
      Ember.set(field, 'horiClass', 'col-xs-12');
      Ember.set(field, 'inputClass', 'col-xs-12');
      if (field.extra === undefined) {
        Ember.set(field, 'extra', {});
      }
      if (field.validation === undefined) {
        Ember.set(field, 'validations', {'presence': false});
      } else {
        Ember.set(field, 'validations.presence', false);
      }
      Ember.set(field, 'extra.clearable', true);
      rv.push(field);
    });
    return rv;
  }),

  showFilters: Ember.computed('search_enabled', 'filter_fields', 'filter_fields[]', function() {
    return this.attrs.model === undefined && (this.get('search_enabled') || this.get('filter_fields.length') > 0);
  }),

  listClassName: Ember.computed('showFilters', function() {
    const rv = 'col-sm-12 col-md-8 col-lg-9 col-md-pull-4 col-lg-pull-3';
    if (this.get('showCFLegend') === true && this.get('legends.length') > 0) {
      return rv;
    }
    if (this.get('showFilters') === true) {
      return rv;
    }
    return 'col-xs-12';
  }),

  fields: [
    {
      'name': '__str__',
      'widget': 'text',
      'read_only': true,
      'label': 'Object'
    }
  ],
  list_display: [
    '__str__'
  ],
  list_editable: [],

  display_fields: Ember.computed('list_display[]', 'fields[]', 'list_display', 'fields', 'list_editable', 'list_editable[]', function() {
    const fields = this.get('fields');
    const list_display = this.get('list_display');
    const list_editable = this.get('list_editable');
    const rv = Ember.A();

    if (list_display) {
      list_display.forEach((field_name) => {
        if (fields && fields !== undefined) {
          if (typeof field_name === 'object' && !Ember.isEmpty(field_name)) {
            if (!field_name.hasOwnProperty('readonly') && list_editable.indexOf(Ember.get(field_name, 'name'))) {
              const field = Ember.copy(field_name);
              Ember.set(field, 'readonly', true);
              rv.push(field);
            } else {
              rv.push(field_name);
            }
          } else {
            const filtered = fields.filterBy('name', field_name);
            if (filtered.length > 0) {
              const field = Ember.copy(filtered[0]);
              if (list_editable.indexOf(field_name) === -1) {
                Ember.set(field, 'readonly', true);
              }
              rv.push(field);
            }
          }
        }
      });
    }

    return rv;
  }),

  column_count: Ember.computed('list_display', 'list_display.length', 'customActions.length', 'changeFormRoute', 'app_name', 'label', 'orderable', function() {
    const rv = parseInt(this.get('list_display.length')) +
           parseInt(this.get('customActions.length')) +
           1 +
           parseInt(this.get('allowBulk')? 1 : 0) +
           parseInt(this.get('changeFormRoute') && this.get('app_name') && this.get('label') ? 1 : 0) +
           parseInt(this.get('orderable') ? 1 : 0)
    ;
    return rv;
  }),

  title: Ember.computed('label', function() {
    const inflector = this.get('inflector');
    const label = this.get('label');
    return inflector.pluralize(label);
  }),

  filterModel(search, filters, pageNumber) {
    if (this.get('model_name')) {
      const limit = this.get('limit');
      this.set('currentPage', pageNumber);
      let rv;
      if (Ember.isEmpty(search) && filters === {} && pageNumber === 1 && limit === null)  {
        rv = this.get('store').findAll(this.get('model_name'));
      } else {
        const criteria = {search, ...filters, page: pageNumber};
        if (limit) {
          criteria.page_size = limit;
        }
        rv = this.get('store').query(this.get('model_name'), criteria);
      }
      rv.then((result) => {
        if (!this.get('isDestroyed') && !this.get('isDestroying')) {
          this.set('meta', result.get('meta'));
        }
      });
      this.set('model', rv);
      return rv;
    }
    return [];
  },

  allChecked: Ember.computed('model', 'model.@each._selected', {
    get() {
      const model = this.get('model');
      if (!model) {
        return true;
      }
      return model.every((record) => {
        return record.get('_selected');
      });
    },
    set(key, value) {
      const model = this.get('model');
      if (model) {
        model.map((record) => {
          record.set('_selected', value);
        });
      }
      return value;
    }
  }),

  noneChecked: Ember.computed('model', 'model.@each._selected', function() {
    const model = this.get('model');
    if (!model) {
      return true;
    }
    return !model.any((record) => {
      return record.get('_selected');
    });
  }),

  selectedRecords: Ember.computed('sorted', 'sorted.length', 'sorted.@each._selected', function() {
    const records = this.get('sorted');
    return records.filter((record) => {
      return record.get('_selected');
    });
  }),

  selectedRecordsForAction: Ember.computed('selectedRecords', 'selectedBulkAction', function() {
    const condition = this.get('selectedBulkAction.display_condition');
    const selectedRecords = this.get('selectedRecords');
    if (!condition) {
      return selectedRecords;
    }

    const propertyPath = condition.property_path;
    return selectedRecords.filter((record) => {
      const value = record.get(propertyPath);
      return this.evaluate(condition, value);
    });
  }),

  ignoredRecordsForAction: Ember.computed('selectedRecordsForAction', 'selectedBulkAction', function() {
    const condition = this.get('selectedBulkAction.display_condition');
    if (!condition) {
      return null;
    }

    const selectedRecords = this.get('selectedRecords');
    const selectedRecordsForAction = this.get('selectedRecordsForAction');
    const selectIds = selectedRecordsForAction.map((record) => { return record.get('id'); });
    const rv = selectedRecords.filter((record) => {
      return selectIds.indexOf(record.get('id')) === -1;
    });

    if (rv.length === 0) {
      return null;
    }

    return rv;
  }),

  listActions: Ember.computed('list_actions', 'list_actions.length', function() {
    const rv = new Ember.A();

    const list_actions = this.do_filter_actions(this.get('list_actions'), this.get('actionsFilters'), this.get('actionsExcludes'));
    if (list_actions) {
      list_actions.map((action) => {
        rv.pushObject(action);
      });
    }

    return rv;
  }),

  sortActionsBy: ['position'],
  sortedActions: Ember.computed.sort('listActions', 'sortActionsBy'),

  bulkActions: Ember.computed('customActions', 'customActions.length', 'customActions.@each.allowBulk', 'bulk_actions', 'bulk_actions.length', function() {
    const rv = new Ember.A();
    if (this.get('allowDelete')) {
      rv.pushObject({
        text: 'Delete',
        type: 'closureMethod',
        method: '_do_delete_record',
        btn_class: 'btn btn-danger',
        icon_class: 'fa fa-trash',
      });
    }

    const customActions = this.get('customActions');
    if (customActions) {
      customActions.map((action) => {
        if (action.allowBulk && action.type !== 'download') {
          rv.pushObject(action);
        }
      });
    }

    const bulk_actions = this.do_filter_actions(this.get('bulk_actions'), this.get('actionsFilters'), this.get('actionsExcludes'));
    if (bulk_actions) {
      bulk_actions.map((action) => {
        rv.pushObject(action);
      });
    }

    return rv;
  }),

  selectedBulkAction: null,

  legends: Ember.computed('conditionalFormat', function() {
    const cf = this.get('conditionalFormat');
    const rv = [];
    let prop;
    for (prop in cf) {
      if (cf.hasOwnProperty(prop)) {
        cf[prop].forEach((item) => {
          if (!Ember.isEmpty(item.description)) {
            rv.push({class: prop, description: item.description});
          }
        });
      }
    }
    return rv;
  }),

  onModelNameChange: Ember.observer('model_name', function() {
    this.loadModel();
  }),

  loadModel() {
    this.filterModel(this.get('_search_value'), this.get('_filter_values'), 1);
    if (this.get('sortableBy')) {
      this.set('sortBy', [this.get('sortableBy')]);
    }
  },

  setup() {
    this.set('_initial_filters', this.get('_filter_values'));
    if (this.get('loadInitialFiters') === true) {
      this.filterValueChanged(null, null);
    }
    if (this.attrs.model !== undefined) {
      return;
    }
    if (this.get('model') !== undefined) {
      return;
    }
    this.loadModel();
  },

  onModelsChanged: Ember.observer('supervisor.changed_models', function() {
    const models = this.get('supervisor.changed_models');
    const inflector = this.get('inflector');
    const me = inflector.pluralize(this.get('model_name'));
    if (models.indexOf(me) !== -1) {
      this.loadModel();
    }
  }),

  didReceiveAttrs() {
    if (this.get('initOnAttrRec') === true) {
      this.setup();
    }
  },

  init() {
    this._super();
    if (this.get('initOnAttrRec') === false) {
      this.setup();
    }
  },

  resetFilters: Ember.observer('model_name', function() {
    this.set('_search_value', '');
    this.set('_filter_values', {});
  }),

  _do_delete_record(record) {
    const recordName = record.get('__str__');
    return record.destroyRecord().then(() => {
      this.get('flashMessages').success(`${recordName} was sucessfully deleted`);
    }).catch((err) => {
      let message = '';
      err.errors.forEach((error) => {
        if (error.message) {
          message += ` ${error.message}`;
        }
      });
      if (message === '') {
        message = `${recordName} could not be deleted`;
      }
      this.get('flashMessages').danger(message);
      console.error(err);
      record.rollback();
    });
  },

  confirm_delete: task(function * () {
    const record = this.get('modalRecord');
    yield this._do_delete_record(record).finally(() => {
      this.set('modalOpen', false);
      this.set('confirmType', undefined);
      this.set('modalTitle', undefined);
      this.set('modalMessage', undefined);
      this.set('modalRecord', undefined);
    });
  }),

  confirm_bulk: task(function * () {
    const act = this.get('modalRecord');
    const promises = new Ember.A();
    if (act.type === 'request') {
      const { url, atOnce } = act;
      if (atOnce === true) {
        const qs = this.get('selectedRecordsForAction').map((record) => {
          return `ids[]=${record.get('id')}`;
        }).join('&');
        promises.pushObject(this.doRqst(url, act, qs));
      } else {
        this.get('selectedRecordsForAction').forEach((record) => {
          promises.pushObject(this.doRqst(url.replace(':id', record.get('id')), act, record));
        });
      }
    } else {
      this.get('selectedRecordsForAction').forEach((record) => {
        promises.pushObject(this._do_meth(act, this, record));
      });
    }
    yield Ember.RSVP.allSettled(promises).finally(() => {
      this.set('modalOpen', false);
      this.set('allChecked', false);
      this.set('confirmType', undefined);
      this.set('modalTitle', undefined);
      this.set('modalMessage', undefined);
      this.set('modalRecord', undefined);
      this.set('selectedBulkAction', undefined);
      this.set('confirmAggregate', false);
      if (act.type === 'closureMethod') {
        this.filterModel(this.get('_search_value'), this.get('_filter_values'), this.get('currentPage'));
      }
    });
  }),

  modalButtonsDisabled: Ember.computed.alias('confirm_delete.isRunning'),

  filterValueChanged(property, value) {
    const filterValues = this.get('filterValues');
    const rv = {};
    const fields = this.get('filter_fields');

    fields.forEach((filter) => {
      let filter_name = filter;
      let isRelation = false;
      let queryField = 'id';
      if (typeof filter_name === 'object' && !Ember.isEmpty(filter_name)) {
        filter_name = filter.name;
        isRelation = Ember.get(filter, 'extra.isRelation');
        const filterValueField = Ember.get(filter, 'extra.filterValueField');
        if (!Ember.isEmpty(filterValueField)) {
          queryField = filterValueField;
        }
      }
      const prop = filter_name.replace(/_id$/, '');
      const propVal = filterValues.get(prop);

      if (propVal !== false && !Ember.isEmpty(propVal)) {
        if (prop !== filter_name || isRelation) {
          rv[filter_name] = propVal.get(queryField);
        } else {
          rv[prop] = propVal;
        }
      }
    });

    if (value !== false) {
      if (property === 'ordering' || fields.indexOf(property) !== -1 || fields.filter((field) => {
        return typeof field === 'object' && !Ember.isEmpty(field) && field.name === property;
      }).length > 0) {
        rv[property] = value;
      } else if (fields.indexOf(`${property}_id`) !== -1) {
        if (Ember.isEmpty(value)) {
          rv[`${property}_id`] = undefined;
        } else {
          rv[`${property}_id`] = value.get('id');
        }
      }
    } else {
      rv[property] = undefined;
    }
    this.set('_filter_values', rv);
  },

  onFilterValuesChange: Ember.observer('_filter_values', function() {
    Ember.run.debounce(this, () => {
      const filter_val = this.get('_filter_values');
      // if (filter_val === rv) {
      this.filterModel(this.get('_search_value'), filter_val, 1);
      // }
    }, 250);
  }),

  orderingFields: Ember.computed('ordering_fields', function() {
    const rv = [];
    this.get('ordering_fields').forEach((field) => {
      if (typeof field === 'object' && !Ember.isEmpty(field)) {
        rv.push(field);
      } else {
        rv.push({field, filter: field});
      }
    });
    return rv;
  }),

  actions: {
    toggleOrdering(order) {
      const currentOrdering = this.get('_filter_values.ordering');
      if (currentOrdering === order) {
        order = `-${order}`;
      }
      this.set('filterValues.ordering', order);
      this.filterValueChanged('ordering', order);
    },

    reorderItems(items) {
      items.forEach((item, index) => {
        item.set(this.get('sortableBy'), index+1);
        item.save();
      });
    },

    deleteRecord(record) {
      const model_name = this.get('model_name');
      const parts = model_name.split('/');
      this.set('confirmType', 'delete');
      this.set('modalTitle', `Confirm delete ${parts[1]} ${record.get('__str__')}`);
      this.set('modalMessage', `Please confirm you want to delete ${record.get('__str__')}`);
      this.set('modalRecord', record);
      this.set('modalOpen', true);
    },

    cancel_confirm() {
      this.set('modalOpen', false);
      this.set('modalTitle', undefined);
      this.set('modalMessage', undefined);
      this.set('modalRecord', undefined);
    },

    bulkAction(act) {
      const model_name = this.get('model_name');
      const parts = model_name.split('/');
      let modelName = parts[1];
      let count = this.get('selectedRecordsForAction.length');
      if (count > 1) {
        const inflector = this.get('inflector');
        modelName = inflector.pluralize(parts[1]);
      }
      this.set('confirmType', 'bulk');
      this.set('confirmAggregate', act.aggregate);
      this.set('modalTitle', `Confirm "${act.text}" ${count} ${modelName}`);
      this.set('modalMessage', `Please confirm you want to "${act.text}" ${count} ${modelName}`);
      if (this.get('ignoredRecordsForAction')) {
        count = this.get('ignoredRecordsForAction.length');
        let have, they;
        if (count > 1) {
          const inflector = this.get('inflector');
          modelName = inflector.pluralize(parts[1]);
          have = 'have';
          they = 'they don\'t';
        } else {
          modelName = parts[1];
          have = 'has';
          they = 'it doesn\'t';
        }
        this.set('ignoreMessage', `${count} ${modelName} ${have} been ignored as ${they} meet "${act.text}"'s conditions`);
      }
      this.set('modalRecord', act);
      this.set('modalOpen', true);
    },

    previousPage() {
      this.filterModel(this.get('_search_value'), this.get('_filter_values'), parseInt(this.get('currentPage')) - 1);
    },

    nextPage() {
      this.filterModel(this.get('_search_value'), this.get('_filter_values'), parseInt(this.get('currentPage')) + 1);
    },

    goToPage(page) {
      this.filterModel(this.get('_search_value'), this.get('_filter_values'), page);
    }
  },
});
