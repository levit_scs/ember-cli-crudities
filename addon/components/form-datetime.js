import Ember from 'ember';
import layout from '../templates/components/form-datetime';
import BoundValueMixin from'../mixins/boundvalue';

export default Ember.Component.extend(BoundValueMixin, {
  layout,
  tagName: undefined,
  type: 'datetime',
  date: Ember.computed('value', {
    get() {
      let dt = this.get('value');
      if (Ember.isEmpty(dt)) {
        return undefined;
      }
      dt = new Date(dt);
      let month = dt.getMonth()+1;
      if (month < 10) {
        month = `0${month}`;
      }
      let day = dt.getDate();
      if (day < 10) {
        day = `0${day}`;
      }
      const rv = `${dt.getFullYear()}-${month}-${day}`;
      return rv;
    },
    set(key, value) {
      if (!Ember.isEmpty(value)) {
        const components = value.split('-');
        let oldDT = this.get('value');
        if (Ember.isEmpty(oldDT)) {
          oldDT = new Date(0);
        } else {
          oldDT = new Date(oldDT);
        }
        oldDT.setFullYear(components[0]);
        oldDT.setMonth(components[1] - 1);
        oldDT.setDate(components[2]);
        this.set('value', oldDT.toISOString());
      }
      return value;
    }
  }),
  time: Ember.computed('value', {
    get() {
      let dt = this.get('value');
      if (Ember.isEmpty(dt)) {
        return undefined;
      }
      dt = new Date(dt);
      let hours = dt.getHours();
      if (hours < 10) {
        hours = `0${hours}`;
      }
      let minutes = dt.getMinutes();
      if (minutes < 10) {
        minutes = `0${minutes}`;
      }
      const rv = `${hours}:${minutes}`;
      return rv;
    },
    set(key, value) {
      if (!Ember.isEmpty(value)) {
        const components = value.split(':');
        let oldDT = this.get('value');
        if (Ember.isEmpty(oldDT)) {
          oldDT = new Date(0);
        } else {
          oldDT = new Date(oldDT);
        }
        oldDT.setHours(components[0]);
        oldDT.setMinutes(components[1]);
        this.set('value', oldDT.toISOString());
      }
      return value;
    }
  }),

  actions: {
    validate(parent) {
      parent.validate();
    },
  },
});
