import Ember from 'ember';
import ForeignKeyBase from './foreignkey-base';
import layout from '../templates/components/form-manytomany-lists';

export default ForeignKeyBase.extend({
  layout,
  type: 'manytomany',

  selectedAvailable: new Ember.A(),
  selectedValue: new Ember.A(),
  autocomplete: true,
  _search: 'simple',
  filterValue: undefined,

  available: Ember.computed('choices', 'value', {
    get() {
      const value = this.get('value');
      let valueIds;
      if (value) {
        valueIds  = value.map((val) => {
          return String(val.get('id'));
        });
      } else {
        valueIds = new Ember.A();
      }

      const choices = this.get('choices');
      return new Ember.A(choices.filter((item) => {
        return valueIds.indexOf(String(item.get('id'))) === -1;
      }));
    },
    set(key, value) {
      return new Ember.A(value);
    }
  }),

  _clean(content) {
    if (content === undefined) {
      content = this.get('content');
    }
    content.then((items) => {
      const possibleIds = items.map((item) => {
        return String(item.get('id'));
      });
      this.set('value', this.get('value').filter((item) => {
        const id = String(item.get('id'));
        return possibleIds.indexOf(id) !== -1;
      }));
    });

  },

  changeSelection(container, selection) {
    const selected = [];
    for (let i=0; i < selection.target.selectedOptions.length; i++) {
      selected.push(selection.target.selectedOptions[i].value);
    }
    const choices = this.get('choices');
    this.set(container, choices.filter((item) => {
      return selected.indexOf(item.get('id')) !== -1;
    }));
  },

  actions: {
    changeSelectedAvailable(selection) {
      this.changeSelection('selectedAvailable', selection);
    },
    changeSelectedValue(selection) {
      this.changeSelection('selectedValue', selection);
    },
    move(origin, target) {
      const targetItems = this.get(target);
      const originItems = this.get(origin);
      if (this.get('search') === 'simple' || target !== 'available') {
        originItems.forEach((item) => {
          targetItems.pushObject(item);
        });
      }
      if (origin === 'selectedValue' || origin === 'value') {
        const ids = originItems.map((item) => {
          return String(item.get('id'));
        });
        this.set('value', this.get('value').filter((item) => {
          const id = String(item.get('id'));
          return ids.indexOf(id) === -1;
        }));
      }
    },
    filter(e) {
      this.send('search', e.target.value);
    }
  },
});
