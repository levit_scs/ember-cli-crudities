import Ember from 'ember';
import Component from '@ember/component';
import layout from '../templates/components/form-duration';
import { seconds_to_str, str_to_seconds } from '../utils/duration';

export default Component.extend({
  layout,
  _debounce: 500,
  _value: null,
  debounce: Ember.computed.or('field.extra.debounce', '_debounce'),

  setValue() {
    const value = this.get('_value');
    this.set('value', str_to_seconds(value, this.get('extra.withSeconds')));
  },

  displayValue: Ember.computed('value', {
    get() {
      const rv = seconds_to_str(this.get('value'), this.get('extra.withSeconds'));
      return rv;
    },
    set(key, value) {
      this.set('_value', value);
      Ember.run.debounce(this, this.setValue, this.get('debounce'));
      return value;
    }
  }),

  validate(widget) {
    return widget.validate();
  },
});
