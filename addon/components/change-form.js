import Ember from 'ember';
import { task } from 'ember-concurrency';
import layout from '../templates/components/change-form';
import WithCustomActionsMixin from '../mixins/with-custom-actions';
import ConfigurableLoaderMixin from '../mixins/configurable-loader';
import WithValidationMixin from '../mixins/with-validation';
import filterableActionsMixin from '../mixins/filterable-actions';
import withDefaults from '../mixins/with-defaults';
import conditionalFormat from '../utils/conditional-format';

export default Ember.Component.extend(filterableActionsMixin, WithValidationMixin, WithCustomActionsMixin, ConfigurableLoaderMixin, withDefaults, {
  layout,
  classNames: ['change-form', ],
  store: Ember.inject.service(),
  _routing: Ember.inject.service('-routing'),
  flashMessages: Ember.inject.service(),
  inflector: new Ember.Inflector(Ember.Inflector.defaultRules),
  initOnAttrRec: true,

  registry: Ember.A(),

  modalOpen: false,
  modalRecord: undefined,
  modalMeta: undefined,
  modalTitle: undefined,
  modelField: undefined,
  isModal: false,
  performing: false,
  saveMes: [],

  defaults: {},
  fieldsets: [{
    title: null,
    fields: [
      {
        name: '__str__',
        label: 'Object',
        read_only: true,
        widget: 'input',
      }
    ]
  }],
  fetch_meta: false,
  allowDelete: true,
  allowEdit: true,
  expandActions: true,
  isEmbedded: false,

  conditionalFormatting: null,

  doFormat() {
    const m = this.get('model');
    if (m.then) {
      m.then((model) => {
        const conditions = this.get('conditionalFormat');
        const formatting = conditionalFormat(model, conditions);
        if (!Ember.isEmpty(formatting)) {
          this.set('conditionalFormatting', formatting);
        } else {
          this.set('conditionalFormatting', null);
        }
      });
    }
  },

  onModelChange: Ember.observer('model', function() {
    this.doFormat();
  }),

  _get_defaults_for_fieldset(fields, rv) {
    fields.forEach((field) => {
      const def = this.get_default_for_field(field);
      if (def) {
        rv[Ember.get(field, 'name')] = def;
      }
      const subfields = Ember.get(field, 'fields');
      if (subfields) {
        this._get_defaults_for_fieldset(subfields, rv);
      }
      const tabs = Ember.get(field, 'tabs');
      if (tabs) {
        tabs.forEach((tab) => {
          this._get_defaults_for_fieldset(Ember.get(tab, 'fields'), rv);
        });
      }
    });
  },

  getDefaults() {
    const rv = this.get('defaults');
    if (this.get('sortableBy')) {
      rv[this.get('sortableBy')] = 0;
    }
    const fieldsets = this.get('fieldsets');
    fieldsets.forEach((fieldset) => {
      fieldset = new Ember.Object(fieldset);
      this._get_defaults_for_fieldset(fieldset.get('fields'), rv);
    });
    return rv;
  },

  didReceiveAttrs() {
    if (this.get('initOnAttrRec') === true) {
      this.setup();
    }
  },

  init() {
    this._super();
    if (this.get('initOnAttrRec') === false) {
      this.setup();
    }
  },

  setupTask: task(function * () {
    if (this.attrs.model !== undefined) {
      const appConfig = Ember.getOwner(this).resolveRegistration('config:environment');
      const forceReload = Ember.get(appConfig, 'ember-cli-crudities.forceReloadModelForm');
      if (forceReload && !this.get('dontReload') && this.get('model.id')) {
        this.get('model').reload();
      }
      return;
    }
    let id = this.get('recordId');
    if (id === undefined) {
      id = this.get('id');
    }
    if (id !== undefined && id !== -1 && id !== 'new') {
      this.set('model', this.get('store').findRecord(this.get('model_name'), id));
    } else {
      const defaults = this.getDefaults();
      const model_name = this.get('model_name');
      if (Ember.isEmpty(model_name)) {
        Ember.run.later(this, this.setup, 250);
        return;
      }
      try {
        const new_model = this.get('store').createRecord(
          model_name,
          defaults
        );
        this.set('model', new_model);
      } catch (err) {
        if (err.message && err.message.match(/pass a model name to the store/)) {
          Ember.run.later(this, this.setup, 250);
          return;
        }
      }
      const initFlag = this.get('initFlag');
      if (!Ember.isEmpty(initFlag)) {
        this.set(`model.${initFlag}`, true);
      }
      for (const k in defaults) {
        if (defaults.hasOwnProperty(k) && k !== 'id') {
          const val = defaults[k];
          if (val !== this.get(`model.${k}`)) {
            if (!Ember.isEmpty(val) && val.hasOwnAttribute && (val.hasOwnAttribute('isFulfilled') || val.then)) {
              const v = yield val;
              if (!this.get('isDestroyed') && !this.get('isDestroying')) {
                this.set(`model.${k}`, v);
              }
            } else if (!this.get('isDestroyed') && !this.get('isDestroying')) {
              this.set(`model.${k}`, val);
            }
          }
        }
      }
      if (!Ember.isEmpty(initFlag)) {
        this.set(`model.${initFlag}`, false);
      }
    }
    this.doFormat();
  }).drop(),

  setup() {
    this.get('setupTask').perform();
  },

  register(/* collection */) {
    // should be removed, needs chek
  },

  unregister(/* collection */) {
    // should be removed, needs chek
  },

  do_redirect(after) {
    const routing = this.get('_routing');
    const model_name = this.get('model_name').split('/');
    if (after === 'redirect') {
      const changeListRoute = this.get('changeListRoute');
      if (changeListRoute === undefined) {
        routing.transitionTo('index');
      } else {
        routing.transitionTo(changeListRoute, model_name);
      }
    } else {
      const id = this.get('model.id');
      if (id !== this.attrs.id) {
        model_name[2] = id;
        routing.transitionTo(routing.get('currentRouteName'), model_name);
      }
    }
  },

  after_save(model, after) {
    this.do_redirect(after, model);
  },

  after_delete() {
    this.do_redirect('redirect', false);
  },

  after_cancel() {
    this.do_redirect('redirect', this.get('model'));
  },

  _get_attr(model, path) {
    let target;
    if (isNaN(parseInt(path[0]))) {
      target = Ember.get(model, path[0]);
    } else {
      target = model.objectAt(parseInt(path[0]));
    }
    if (path.length === 1) {
      return {model, value: target, property: path[0]};
    }
    return this._get_attr(target, path.slice(1));
  },

  save_all: task(function * (after) {
    this.set('performing', after);
    const promise = new Ember.RSVP.Promise((resolve, reject) => {
      this.validate(() => {
        this._do_for_all(this.get('model'), this.get('fieldsets'), 'save', this.get('saveTwice'), undefined, undefined, true).then((model) => {
          this.get('flashMessages').success(`${this.get('model.__str__')} was sucessfully saved`);
          this.after_save(model, after);
          resolve(model);
        }, (err) => {
          let message = [];
          const errors = Ember.get(err, 'errors');
          errors.forEach((error) => {
            const pointer = Ember.get(error, 'source.pointer');
            if (pointer === '/data/attributes/0' || pointer === '/data') {
              message.push(error.detail);
            } else if (this.get('isEmbedded') === true) {
              const ptr_arr = pointer.split('/');
              if (ptr_arr.length > 4) {
                const rv = this._get_attr(this.get('model'), ptr_arr.slice(3));
                let current_errors = Ember.get(rv.model, `form_errors.${rv.property}`);
                if (current_errors === undefined) {
                  current_errors = [];
                  if (Ember.get(rv.model, 'form_errors') === undefined) {
                    Ember.set(rv.model, 'form_errors', new Ember.Object());
                  }
                }
                current_errors.push(error.detail);
                Ember.set(rv.model, `form_errors.${rv.property}`, current_errors);
                Ember.set(rv.model, 'isError', true);
              }
            }
          });
          if (Ember.isEmpty(message)) {
            message = 'There was an error while saving this record';
          } else {
            message = message.join('\n');
          }
          this.get('flashMessages').danger(message);
          reject(err);
        }).finally(() => {
          this.set('performing', false);
        });
      }, (err) => {
        this.get('flashMessages').danger('Please correct the errors in the form.');
        this.set('performing', false);
        reject(err);
      });
    }).catch((err) => {
      this.get('flashMessages').danger('Please correct the errors in the form.');
    });
    yield promise;
  }),

  cancel_all: task(function * (model) {
    this.set('performing', 'cancel');
    if (model.hasOwnProperty('isFulfilled')) {
      yield model.then((m) => {
        this._do_for_all(m, this.get('fieldsets'), 'rollbackAttributes').then(() => {
          this.after_cancel();
        }).catch((err) => {
          this.get('flashMessages').danger('There was an error rolling back your changes, please try to reload the page');
          console.error(err);
        }).finally(() => {
          this.set('performing', false);
        });
      });
    } else {
      yield this._do_for_all(model, this.get('fieldsets'), 'rollbackAttributes').then(() => {
        this.after_cancel();
      }).catch((err) => {
        this.get('flashMessages').danger('There was an error rolling back your changes, please try to reload the page');
        console.error(err);
      }).finally(() => {
        this.set('performing', false);
      });
    }
  }),

  cancel_related: task(function * () {
    const field = this.get('modalField');
    if (field) {
      const model = this._do_get_model(this.get('modalRecord'));
      const fieldsets = this.get('modalMeta.fieldsets');
      this.set('performing', 'cancel_related');
      yield this._do_for_all(model, fieldsets, 'rollbackAttributes').then(() => {
        this.set('modalOpen', false);
        this.set('modalMeta', undefined);
        this.set('modalTitle', undefined);
        this.set('modelField', undefined);
      }).catch((err) => {
        console.error(err);
      }).finally(() => {
        this.set('performing', false);
      });
    } else {
      this.set('modalOpen', false);
      this.set('modalMeta', undefined);
      this.set('modalTitle', undefined);
      this.set('modelField', undefined);
    }
  }),

  save_related: task(function * () {
    this.set('performing', 'save_related');
    const model = this._do_get_model(this.get('modalRecord'));
    const field = this.get('modalField');
    const fieldsets = this.get('modalMeta.fieldsets');
    yield this._do_for_all(model, fieldsets, 'save').then(() => {
      if (this.get('modalToMany')) {
        field.get('value').pushObject(model);
      } else {
        field.set('value', model);
      }
      this.set('modalOpen', false);
      this.set('modalMeta', undefined);
      this.set('modalTitle', undefined);
      this.set('modelField', undefined);
      this.get('flashMessages').success(`${model.get('__str__')} was sucessfully saved`);
    }).catch((err) => {
      this.get('flashMessages').danger('There was an error while saving this record');
      console.error(err);
    }).finally(() => {
      this.set('performing', false);
    });
  }),

  confirm_related: task(function * () {
    this.set('performing', 'confirm_related');
    const model = this._do_get_model(this.get('modalRecord'));
    const fieldsets = this.get('fieldsets');
    const recordName = model.get('__str__');
    yield this._do_for_all(model, fieldsets, 'destroyRecord').then(() => {
      this.get('flashMessages').success(`${recordName} was sucessfully deleted`);
      this.after_delete();
    }).catch((err) => {
      let message = '';
      err.errors.forEach((error) => {
        if (error.message) {
          message += ` ${error.message}`;
        }
      });
      if (message === '') {
        message = `${recordName} could not be deleted`;
      }
      this.get('flashMessages').danger(message);
      console.error(err);
      model.rollback();
    }).finally(() => {
      this.set('modalOpen', false);
      this.set('modalMessage', false);
      this.set('modalMeta', undefined);
      this.set('modalTitle', undefined);
      this.set('modelField', undefined);
      this.set('performing', false);
    });
  }),

  buttonsDisabled: Ember.computed.or('save_all.isRunning', 'cancel_all.isRunning', 'modalOpen', 'actionIsRunning'),
  modalButtonsDisabled: Ember.computed.or('save_related.isRunning', 'confirm_related.isRunning', 'cancel_related.isRunning', 'actionIsRunning'),

  editRelated(model_name, field, isRecord) {
    let promise;
    if (isRecord) {
      promise = Ember.RSVP.resolve(field);
    } else {
      promise = field.get('value');
    }
    promise.then((record) => {
      this.set('modalRecord', record);
      this.set('modalField', field);
      this.set('modalMessage', false);
      const inflector = this.get('inflector');
      const parts = model_name.split('/');
      this.set('modalTitle', `Edit ${parts[1]} ${record.get('__str__')}`);
      parts[1] = inflector.pluralize(parts[1]);
      const modelLoader = this.get('modelLoader');
      modelLoader.get_model_meta(parts[0], parts[1]).then((meta) => {
        this.set('modalMeta', meta);
        this.set('modalOpen', true);
      });
    });
  },

  actions: {

    add_related(model_name, field, toMany, record) {
      /* also check that we received a record and not an event */
      if (record === undefined || !record.hasOwnProperty('id') && !record.hasOwnProperty('isFulfilled')) {
        record = this.get('store').createRecord(model_name, {});
      }
      this.set('modalRecord', record);
      this.set('modalField', field);
      this.set('modalMessage', false);
      const inflector = this.get('inflector');
      const parts = model_name.split('/');
      this.set('modalTitle', `Add ${parts[1]}`);
      parts[1] = inflector.pluralize(parts[1]);
      const modelLoader = this.get('modelLoader');
      modelLoader.get_model_meta(parts[0], parts[1]).then((meta) => {
        this.set('modalMeta', meta);
        this.set('modalToMany', toMany);
        this.set('modalOpen', true);
      });
    },

    edit_related(model_name, field, isRecord) {
      this.editRelated(model_name, field, isRecord);
    },

    delete_all() {
      const record = this.get('model');
      const model_name = this.get('model_name');
      const parts = model_name.split('/');
      this.set('modalField', false);
      this.set('modalRecord', record);
      this.set('modalTitle', `Confirm delete ${parts[1]} ${record.get('__str__')}`);
      this.set('modalMessage', `Please confirm you want to delete ${record.get('__str__')}`);
      this.set('modalOpen', true);
    },
  }

});
