import Ember from 'ember';
import FormInput from './form-input';

export default FormInput.extend({
  types: ['password', 'text'],
  icons: ['', '-slash'],
  typeIndex: 0,
  type: Ember.computed('types', 'typeIndex', function() {
    const types = this.get('types');
    const index = this.get('typeIndex');
    return types[index];
  }),
  actionAddonIcon: Ember.computed('icons', 'typeIndex', function() {
    const icons = this.get('icons');
    const index = this.get('typeIndex');
    return `fa fa-eye${icons[index]}`;
  }),

  actionAddon: 'toggleType',

  actions: {
    toggleType() {
      this.set('typeIndex', 1 - this.get('typeIndex'));
    }
  }
});
