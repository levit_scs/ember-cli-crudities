import Component from '@ember/component';
import layout from '../templates/components/cl-legend';

export default Component.extend({
  layout,
  tagName: 'table',
  classNames: 'table'
});
