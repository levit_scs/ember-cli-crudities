import Ember from 'ember';
import layout from '../templates/components/form-foreignkey';
import ForeignKeyBase from './foreignkey-base';
import BoundValueMixin from '../mixins/boundvalue';

export default ForeignKeyBase.extend(BoundValueMixin, {
  layout,
  type: 'foreignkey',
  isExpanded: false,

  _validate_current_selection(content) {
    const valueId = `${this.get('value.id')}`;
    let firstId = undefined;
    let first;
    /* this console.log is a weird workaround but it works
     * if the values in that console.log are not accessed prior the code after it
     * things break in edit forms */
    console.log({content});
    if (this.get('extra.autoSetUnique') && !Ember.isEmpty(content) && parseInt(Ember.get(content, 'length')) === 1 || this.get('extra.autoFirstAfterChange') && !Ember.isEmpty(content)) {
      try {
        first = content.firstObject;
        firstId = first.get('id');
      } catch (e) {
        first = content[0];
        if (!Ember.isEmpty(first)) {
          firstId = first.id;
        }
      }
      firstId = String(firstId);
    }
    if (firstId !== 'undefined' && (this.get('extra.autoSetUnique') && !Ember.isEmpty(content) && parseInt(Ember.get(content, 'length')) === 1 || this.get('extra.autoFirstAfterChange') && !Ember.isEmpty(content)) && firstId !== valueId) {
      if (!this.get('isDestroyed')) {
        this.set('value', first);
      }
    } else if (!this.get('extra.dontSeed') && (valueId !== 'undefined' && (Ember.isEmpty(content) || !content.any((record) => {
      return `${record.get('id')}` === valueId;
    })))) {
      if (!this.get('isDestroyed')) {
        this.set('value', undefined);
      }
    }
  },

  _do_search(term, clean) {
    const rv = this._super(term, clean);
    if (!this.get('isDestroyed')) {
      if (rv.then) {
        rv.then((results) => {
          this._validate_current_selection(results);
        });
      } else {
        this._validate_current_selection(rv);
      }
    }
    return rv;
  }
});
