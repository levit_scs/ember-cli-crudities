import Ember from 'ember';
import layout from '../templates/components/cl-aggregates';

export default Ember.Component.extend({
  layout,
  tagName: 'tfoot',
});
