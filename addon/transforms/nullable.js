import Ember from 'ember';
import DS from 'ember-data';

export default DS.Transform.extend({
  deserialize(serialized) {
    return serialized;
  },

  serialize(deserialized) {
    if (Ember.isEmpty(deserialized)) {
      return null;
    }
    return deserialized;
  }
});
