import Ember from 'ember';
import DS from 'ember-data';
import { str_to_seconds } from '../utils/duration';

export default DS.Transform.extend({
  deserialize(serialized) {
    return str_to_seconds(serialized, true);
  },

  serialize(deserialized) {
    return deserialized;
  }
});
