import Ember from 'ember';

function leftPad(value, length) {
  if (Ember.isEmpty(length)) {
    length = 2;
  }
  value = value.toString();

  while (value.length < length) {
    value = `0${value}`;
  }

  return value;
}

export function seconds_to_str(value, withSeconds, separator) {
  if (Ember.isEmpty(separator)) {
    separator = ' ';
  }
  const duration = window.moment.duration(value, 'seconds');
  let rv = '';
  // if (duration.years()) {
  //   rv = `${duration.years()} `;
  // }
  // if (duration.months()) {
  //   rv += `${duration.months()} `;
  // }
  // if (duration.weeks()) {
  //   rv += `${duration.weeks()} `;
  // }
  if (duration.days()) {
    rv += `${parseInt(Math.floor(duration.asDays()))}${separator}`;
  }
  rv += `${leftPad(duration.hours())}:${leftPad(duration.minutes())}`;
  if (withSeconds) {
    rv += `:${leftPad(duration.seconds())}`;
  }

  return rv;
}

export function str_to_seconds(value, withSeconds) {
  let duration = value;
  if (Ember.isEmpty(value)) {
    duration = '0';
  }
  if (value === '0.0') {
    duration = '0';
  }
  duration = duration.replace(' ', '.');

  if (!withSeconds) {
    duration += ':0';
  }

  // return window.moment.duration(duration);

  const timeChunks = duration.split(':');
  if (timeChunks.length === 1) {
    return parseInt(duration);
  }
  let total = parseInt(timeChunks[1]) * 60;
  if (withSeconds) {
    total += parseInt(timeChunks[2]);
  }
  const daysHours = timeChunks[0].split('.');
  if (daysHours.length === 1) {
    total += parseInt(timeChunks[0]) * 3600;
  } else {
    total += parseInt(daysHours[0]) * 3600 * 24 + parseInt(daysHours[1]) * 3600;
  }

  return total;
}
