import Ember from 'ember';


const Validator = Ember.Object.extend({
  validations: {},
  isValid: true,
  errors: new Ember.A(),

  validate_presence(value, options) {
    if (options !== false && value !== undefined && value !== null && value.then) {
      if (Ember.isBlank(value.content)) {
        return ['This field may not be blank'];
      }
    } else if (options !== false && Ember.isBlank(value)) {
      return ['This field may not be blank'];
    }
    return [];
  },

  validate_length(value, options) {
    const { minimum, maximum } = options;
    const errors = [];
    if (minimum !== undefined && Number.isInteger(minimum) && (Ember.isEmpty(value) || value.length < minimum)) {
      errors.push(`This field must be at least ${minimum} characters long`);
    }
    if (maximum !== undefined && Number.isInteger(maximum) && !Ember.isEmpty(value) && value.length > maximum) {
      errors.push(`This field must be at most ${maximum} characters long`);
    }
    return errors;
  },

  validate(value) {
    const validations = this.get('validations');
    let isValid = true;
    const errors = new Ember.A();
    let result, k;

    for(k in validations) {
      if (this.get(`validate_${k}`) !== undefined) {
        result = this[`validate_${k}`](value, validations[k]);
        isValid = isValid && result.length === 0;
        result.forEach((error) => {
          errors.pushObject(error);
        });
      }
    }

    this.set('isValid', isValid);
    this.set('errors', errors);
    return isValid;
  },
});

export default Validator;

export function validate_model(model, fieldset) {
  return new Ember.RSVP.Promise((resolve, reject) => {
    if (fieldset === undefined) {
      fieldset = [];
    }
    let isValid = true;
    let validations;
    let validator;
    let rv;
    fieldset.forEach((field) => {
      if (Ember.get(field, 'name') !== undefined) {
        validations = Ember.get(field, 'validations');
        if (Ember.get(field, 'required')) {
          Ember.set(validations, 'presence', true);
        }
        if (validations !== undefined) {
          validator = Validator.create({validations});
          rv = validator.validate(Ember.get(model, Ember.get(field, 'name')));
          const formErrors = new Ember.Object();
          const oldErrors = model.get('form_errors');
          if (!Ember.isEmpty(oldErrors)) {
            for(const k in oldErrors) {
              if (oldErrors.hasOwnProperty(k)) {
                Ember.set(formErrors, k, Ember.get(oldErrors, k));
              }
            }
          }
          Ember.set(formErrors, Ember.get(field, 'name'), validator.get('errors'));
          Ember.set(model, 'form_errors', formErrors);
          if (!rv) {
            isValid = false;
          }
        }
      }
    });
    if (isValid) {
      resolve(model);
    } else {
      console.warn('errors found', fieldset, model.get('form_errors'));
      reject(new Error('Please correct the errors on this form'));
    }
  });
}
