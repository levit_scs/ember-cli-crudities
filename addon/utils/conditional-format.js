import evaluate from './evaluator';

export default function conditionalFormat(model, conditions) {
  let prop;
  let propval;
  let priority;
  let i;

  const rv = [];

  for (prop in conditions) {
    if (conditions.hasOwnProperty(prop)) {
      for (i=0; i<conditions[prop].length; i++) {
        propval = model.get(conditions[prop][i].property_path);
        priority = conditions[prop][i].priority;
        if (evaluate(conditions[prop][i], propval)) {
          rv.push({value: prop, priority: priority?priority:-1});
        }
      }
    }
  }

  if (rv.length > 0) {
    rv.sort((a, b) => { return b.priority - a.priority; });
    return rv[0].value;
  }

  return '';
}
