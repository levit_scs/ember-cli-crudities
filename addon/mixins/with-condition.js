import Ember from 'ember';
import Mixin from '@ember/object/mixin';
import ConditionEvaluatorMixin from './condition-evaluator';

export default Mixin.create(ConditionEvaluatorMixin, {
  observers: [],
  toWatch: [],

  _update_prop(observer, condition, prop) {
    if (Ember.isEmpty(observer) || Ember.isEmpty(condition)) {
      return;
    }
    const value = this.get(observer);
    const rv = this.evaluate(this.get(condition), value);
    this.set(prop, rv);
  },

  didReceiveAttrs() {
    this._super();
    const observers = this.get('observers');
    this.get('toWatch').forEach((toWatchItem) => {
      const toWatch = this.get(`${toWatchItem[0]}.property_path`);
      if (!Ember.isEmpty(toWatch)) {
        const observer = `model.${toWatch}`;
        const fn_name = `_update_${toWatchItem[1]}`;
        this[fn_name] = this._update_prop.bind(this, observer, toWatchItem[0], toWatchItem[1]);
        this.addObserver(observer, this[fn_name]);
        observers.push([observer, fn_name]);
        this[fn_name]();
      }
    });
  },

  willDestroy() {
    this._super();
    this.get('observers').forEach((item) => {
      this.removeObserver(item[0], this[item[1]]);
    });
  },
});
