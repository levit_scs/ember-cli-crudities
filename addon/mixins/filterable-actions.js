import Ember from 'ember';

export default Ember.Mixin.create({
  actionsFilters: null,
  actionsExcludes: null,
  custom_actions: [],

  sortActionsBy: ['position'],
  sortedCustomActions: Ember.computed.sort('customActions', 'sortActionsBy'),

  do_filter_actions(actions, filters, excludes) {
    let filtered = [];
    if (filters) {
      actions.forEach((action) => {
        if (action.slug === undefined) {
          return;
        }
        if (filters.some((filter) => {
          const r = new RegExp(filter);
          return action.slug.match(r);
        })) {
          filtered.push(action);
        }
      });
    } else {
      filtered = actions;
    }
    let rv = [];
    if (excludes) {
      filtered.forEach((action) => {
        if (action.slug === undefined) {
          rv.push(action);
          return;
        }
        if (!excludes.some((filter) => {
          const r = new RegExp(filter);
          return action.slug.match(r);
        })) {
          rv.push(action);
        }
      });
    } else {
      rv = filtered;
    }
    return rv;
  },

  customActions: Ember.computed('custom_actions', 'actionsFilters', 'actionsExcludes', function() {
    return this.do_filter_actions(
      this.get('custom_actions'),
      this.get('actionsFilters'),
      this.get('actionsExcludes')
    );
  })
});
