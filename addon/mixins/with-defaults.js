import Ember from 'ember';
import Mixin from '@ember/object/mixin';

export default Mixin.create({
  get_default_for_field(field) {
    let def = Ember.get(field, 'extra.default');
    if (def) {
      const relatedModel = Ember.get(field, 'extra.related_model');
      if (!Ember.isEmpty(relatedModel)) {
        def = this.get('store').findRecord(relatedModel, def);
      }
    }
    return def;
  }
});
