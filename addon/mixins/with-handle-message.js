import Ember from 'ember';
import Mixin from '@ember/object/mixin';

export default Mixin.create({
  flashMessages: Ember.inject.service(),

  _handleMessage(json) {
    const message = json._message;
    if (Ember.isEmpty(message)) {
      return false;
    }
    if (message.type === 'error') {
      message.type = 'danger';
    }
    this.get('flashMessages')[message.type](message.value);
    return true;
  },
});
