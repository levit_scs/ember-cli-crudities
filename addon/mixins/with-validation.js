import Ember from 'ember';
import { validate_model } from '../utils/validation';
import ConditionEvaluatorMixin from './condition-evaluator';

export default Ember.Mixin.create(ConditionEvaluatorMixin, {

  doCt: 0,

  _do_get_model(value) {
    if (value.hasOwnProperty('isFulfilled')) {
      return value.get('content');
    }
    return value;
  },

  _is_displayed(field, model) {
    const condition = Ember.get(field, 'extra.display_condition');
    if (Ember.isEmpty(condition)) {
      return true;
    }
    return this.evaluate(condition, model.get(Ember.get(condition, 'property_path')));
  },

  _do_method(record, method, resolve, reject) {
    if (method === 'save') {
      if (record !== undefined && record.get('isSoftDeleted') !== true) {
        record[method]().then((saved) => {
          console.log({saved, state: saved.get('currentState')});
          resolve(saved);
        }, (err) => {
          console.log('rejecting', {record, state: record.get('currentState')});
          this.get('flashMessages').danger('record is not saved');
          this.get('flashMessages').danger('record is not saved');
          reject(err);
        });
      } else {
        record.set('isSoftDeleted', false);
        if (!record.get('isDeleted')) {
          record.destroyRecord().then(resolve, reject);
        } else {
          resolve(record);
        }
      }
    } else if (method === 'validate') {
      resolve(record);
    } else {
      record.set('isSoftDeleted', false);
      if (!record.get('isDeleted')) {
        record[method]().then(resolve, reject);
      } else {
        resolve(record);
      }
    }
  },

  _do_for_fields(saved_record, fieldsets, method, top_promises, models, prefix, backlog) {
    const breakException = {};
    fieldsets.forEach((fieldset) => {
      if (fieldset.fields !== undefined) {
        fieldset.fields.forEach((field) => {
          if (!this._is_displayed(field, saved_record)) {
            return;
          }
          if (field.widget && field.widget.split('-')[0] === 'tomany') {
            top_promises.push(new Ember.RSVP.Promise((resolve, reject) => {
              const promises = [];
              const appConfig = Ember.getOwner(this).resolveRegistration('config:environment');
              let foundSaveMe = Ember.get(appConfig, 'ember-cli-crudities.forceSaveUnchanged');
              if (!foundSaveMe) {
                try {
                  this.get('saveMes').forEach((saveMe) => {
                    const parts = [];
                    saveMe.split('.').forEach((part) => {
                      parts.push(part);
                      const name = parts.join('.');
                      if (name === `${prefix}${field.name}`) {
                        foundSaveMe = true;
                        throw breakException;
                      }
                    });
                  });
                } catch (e) {
                  if (e !== breakException) {
                    throw e;
                  }
                }
              }
              if (foundSaveMe) {
                saved_record.get(field.name).then((related) => {
                  related.forEach((related_record) => {
                    if (models.indexOf(related_record) === -1 || backlog.indexOf(related_record) !==1) {
                      promises.push(new Ember.RSVP.Promise((resolve_bottom, reject_bottom) => {
                        this._do_for_all(related_record,
                          [field],
                          method,
                          method === 'save' && field.extra && field.extra.saveTwice,
                          field.name ? `${prefix}${field.name}.` : prefix,
                          backlog,
                          false
                        ).then((saved_related) => {
                          models.push(saved_related);
                          resolve_bottom(related_record);
                          Ember.set(fieldset, 'isError', false);
                        }, (err) => {
                          Ember.set(fieldset, 'isError', true);
                          reject_bottom(err);
                        });
                      }));
                    }
                  });
                }, reject);
              }
              Ember.RSVP.allSettled(promises).then((result) => {
                let allGood = true;
                result.forEach((r) => {
                  if (r.state === 'rejected') {
                    allGood = false;
                  }
                });
                // Upstream bug?
                // sometimes the array received as `result` is empty
                // this isn't the best fix but it works
                if (result.length !== promises.length) {
                  promises.forEach((promise) => {
                    promise.then(resolve, (err) => {
                      Ember.set(fieldset, 'isError', true);
                      reject(err);
                    });
                  });
                } else if (allGood) {
                  resolve(saved_record);
                } else {
                  reject(result);
                }
              }, reject);
            }));
          } else if (field.widget && field.widget === 'foreignkey' && field.extra && field.extra.expand) {
            top_promises.push(new Ember.RSVP.Promise((resolve_bottom, reject_bottom) => {
              saved_record.get(field.name).then((related_record) => {
                this._do_for_all(related_record,
                  [field],
                  method,
                  method === 'save' && field.extra && field.extra.saveTwice,
                  field.name ? `${prefix}${field.name}.` : prefix,
                  backlog,
                  false
                ).then((saved_related) => {
                  models.push(saved_related);
                  resolve_bottom(related_record);
                  Ember.set(fieldset, 'isError', false);
                }, (err) => {
                  Ember.set(fieldset, 'isError', true);
                  reject_bottom(err);
                });
              }, reject_bottom);
            }));
          } else if (field.widget && field.widget === 'fieldset' && this._is_displayed(field, saved_record)) {
            this._do_for_fields(saved_record, [field], method, top_promises, models, prefix);
          } else if (field.widget && field.widget === 'tabset') {
            this._do_for_fields(saved_record, field.tabs, method, top_promises, models, prefix);
          }
        });
        if (method === 'validate' && this._is_displayed(fieldset, saved_record)) {
          top_promises.push(validate_model(saved_record, fieldset.fields));
        }
      } else if (method === 'validate' && this._is_displayed(fieldset, saved_record)) {
        top_promises.push(validate_model(saved_record, fieldset));
      }
    });
  },

  _check_pending_rels(record, fieldsets) {
    let pending = [];
    fieldsets.forEach((fieldset) => {
      if (fieldset.fields !== undefined) {
        fieldset.fields.forEach((field) => {
          if (field.widget && field.widget === 'foreignkey') {
            if (Ember.get(record, field.name) !== undefined && Ember.get(record, `${field.name}.isNew`)) {
              pending.push(Ember.get(record, field.name));
            }
          } else if (field.widget && (field.widget.split('-')[0] === 'manytomany' || Ember.get(field, 'extra.treatAsM2M') === true)) {
            const toRemove = [];
            Ember.get(record, field.name).forEach((related) => {
              if (related !== undefined && Ember.get(related, 'isNew')) {
                pending.push(Ember.get(record, field.name));
              } else if (related !== undefined && Ember.get(related, 'isSoftDeleted') === true) {
                toRemove.push(related);
                Ember.set(related, 'isSoftDeleted', false);
              }
            });
            Ember.get(record, field.name).removeObjects(toRemove);
          } else if (field.widget && field.widget === 'fieldset' && field.fields) {
            pending = pending.concat(this._check_pending_rels(record, [field]));
          } else if (field.widget && field.widget === 'tabset' && field.tabs) {
            pending = pending.concat(this._check_pending_rels(record, [field.tabs]));
          }
        });
      }
    });
    return pending;
  },

  _do_for_all(record, fieldsets, method, twice, prefix, backlog, is_top) {
    if (is_top === undefined) {
      is_top = true;
    }
    if (prefix === undefined) {
      prefix = '';
    }
    if (backlog === undefined) {
      backlog = [];
    }
    if (is_top === true) {
      this.doCt = 0;
    }
    const top_promises = [];
    const models = [];
    let mybacklog = [];
    /** TODO: Can probably be improved with ember-concurency */
    const rv = new Ember.RSVP.Promise((resolve_top, reject_top) => {
      let promise;
      if (method === 'save') {
        mybacklog = this._check_pending_rels(record, fieldsets);
      }
      if (method === 'rollbackAttributes') {
        promise = new Ember.RSVP.Promise((resolve) => {
          if (!Ember.isEmpty(record)) {
            record[method]();
            record.set('isSoftDeleted', false);
          }
          resolve(record);
        });
      } else if (!twice || record !== undefined && record.get('isNew')) {
        if (mybacklog.length === 0) {
          if (record !== null) {
            promise = new Ember.RSVP.Promise((resolve, reject) => {
              if (record.then) {
                record.then((r) => {
                  this._do_method(r, method, resolve, reject);
                }, reject);
              } else {
                this._do_method(record, method, resolve, reject);
              }
            });
          }
          const index = backlog.indexOf(record);
          if (index !== -1) {
            backlog.splice(index, 1);
          }
        } else {
          if (backlog.indexOf(record) === -1) {
            backlog.push(record);
          }
          promise = Ember.RSVP.resolve(record);
        }
      } else {
        promise = Ember.RSVP.resolve(record);
      }
      if (promise !== undefined) {
        promise.then((saved_record) => {
          console.log({saved_record});
          models.push(saved_record);
          this._do_for_fields(saved_record, fieldsets, method, top_promises, models, prefix, mybacklog);
          Ember.RSVP.allSettled(top_promises).then((result) => {
            let allGood = true;
            result.forEach((r) => {
              if (r.state === 'rejected') {
                console.warn(r);
                allGood = false;
              }
            });
            if (!allGood) {
              console.error('error in related records');
              if (this.get('forceValidation') === true) {
                reject_top('error in related records');
                return;
              }
            }
            resolve_top(record);
          }, (err) => {
            console.log('a');
            reject_top(err); });
        }, (err) => {
          console.log('b');
          reject_top(err);
        });
      } else {
        resolve_top(record);
      }
    });
    if (twice) {
      if (mybacklog.length === 0) {
        return new Ember.RSVP.Promise((resolve, reject) => {
          rv.then((record) => {
            record[method]().then((saved_record) => {
              const jndex = backlog.indexOf(record);
              if (jndex !== -1) {
                backlog.splice(jndex, 1);
              }
              resolve(saved_record);
            }, reject);
          }, reject);
        });
      }
      if (backlog.indexOf(record) === -1) {
        backlog.push(record);
      }
    }
    if (mybacklog.length === 0) {
      const jndex = backlog.indexOf(record);
      if (jndex !== -1) {
        backlog.splice(jndex, 1);
      }
    } else {
      if (backlog.indexOf(record) === -1) {
        backlog.push(record);
      }
    }

    return this._final_do(rv, backlog, method);

  },

  _final_do(rv, backlog, method) {
    console.log({method, backlog});
    if (backlog.length > 1) {
      console.log({backlog});
      this.doCt++;
      const newRv = new Ember.RSVP.Promise((resolve, reject) => {
        if (this.doCt > 10) {
          reject('over');
        } else {
          rv.then((saved_record) => {
            Ember.run.later(this, () => {
              this._final_do(rv, backlog).then(resolve, reject);
            }, 250);
          }, reject);
        }
      });
      return newRv;
    }
    return rv;
  },

  validate(callback, errorHandling) {
    return this._do_for_all(this.get('model'), this.get('fieldsets'), 'validate')
      .then(callback, errorHandling);
  }
});
