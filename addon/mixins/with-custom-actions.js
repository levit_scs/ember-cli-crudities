import Ember from 'ember';
import fetch from 'ember-network/fetch';
import { task } from 'ember-concurrency';
import Cookies from 'ember-cli-js-cookie';
import ConfigurableLoader from './configurable-loader';
import { InvalidError } from 'ember-data';
import withDefaults from './with-defaults';
import WithHandleMessage from './with-handle-message';

export default Ember.Mixin.create(ConfigurableLoader, withDefaults, WithHandleMessage, {
  store: Ember.inject.service(),
  modelLoader: Ember.inject.service(),
  flashMessages: Ember.inject.service(),
  receiver: this,
  main: undefined,
  tetherClass: 'modal-dialog modal-lg',
  tetherAttachment: 'middle center',
  tetherTargetAttachment: 'middle center',
  tetherTarget: 'document.body',

  _do_meth(act, receiver, model) {
    const { type, method, text } = act;
    if (type === 'modelMethod') {
      if (model.hasOwnProperty('isFulfilled')) {
        return new Ember.RSVP.Promise((resolve, reject) => {
          model.then((m) => {
            resolve(m[method](act));
            //.then(resolve).catch(reject);
          }).catch((err) => {
            if (!this._handleMessage(err)) {
              this.get('flashMessages').danger(`An error occured while performing "${text}" on ${this.get('model.__str__')}`);
            }
            reject(err);
          });
        });
      }
      return model[method](act).then(() => {
        let receiver = this.get('receiver');
        if (!receiver) {
          receiver = this;
        }
        if (act.refresh_list) {
          try {
            receiver.filterModel(receiver.get('_search_value'), receiver.get('_filter_values'), 1);
          } catch (e) {
            console.error(e);
          }
        }
      });

    }
    return receiver[method](model, act);

  },

  actionIsRunning: Ember.computed.or('method.isRunning', 'rqst.isRunning'),

  method: task(function * (act, receiver, model) {
    const { text } = act;
    this.set('performing', text);
    yield this._do_meth(act, receiver, model);
    // .finally(() => {
    this.set('performing', false);
    // });
  }),

  doRqst(url, act, model, isWizard) {
    let onText = '';
    if (model !== undefined) {
      onText = Ember.get(model, '__str__');
      if (Ember.isEmpty(onText) && isWizard && model.hasOwnProperty('parent')) {
        onText = Ember.get(model, 'parent.__str_');
      }
    }
    if (!Ember.isEmpty(onText)) {
      onText = `on ${onText}`;
    } else {
      onText = '';
    }
    const { verb, pushPayload, text } = act;
    let postfix = '';
    const params = {
      method: verb,
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken'),
        'Content-Type': 'application/json',
      },
      credentials: 'include'
    };
    let body = {};
    if (act.atOnce === true) {
      postfix = `&${model}`;
    } else if (model !== undefined && verb.toUpperCase() === 'POST') {
      body = model.serialize();
      params['body'] = JSON.stringify(body);
    }
    const paramSymbol = url.indexOf('?') !== -1 ? '&' : '?';
    const rv = new Ember.RSVP.Promise((resolve, reject) => {
      fetch(`${url}${paramSymbol}format=json${postfix}`, params).then((response) => {
        if (response.status === 400) {
          response.json().then((json) => {
            if (isWizard) {
              let key;
              for (key in body) {
                model.set(`errors.${key}`, []);
              }
              for (key in json) {
                if (key !== 'non_field_errors' && key !== '_message') {
                  model.set(`errors.${key}`, json[key].map((message) => {
                    return {attribute: key, message};
                  }));
                }
              }
              model.set('isError', true);
            }
            reject(json);
          // }).catch(() => {
          //   reject(response);
          });
        } else if (response.status > 400) {
          reject(response);
        } else if (response.status === 204) {
          resolve({});
        } else {
          response.json().then((json) => {
            resolve(json);
          }).catch(() => {
            reject(response);
          });
        }
      });
    });
    if (pushPayload) {
      return new Ember.RSVP.Promise((resolve, reject) => {
        rv.then((orig) => {
          const json = {...orig};
          let message = {
            type: 'success',
            value: `"${text}" performed successfully ${onText}`
          };
          if (json._message !== undefined) {
            message = json._message;
            delete json._message;
          }
          let triggerSupervisor = true;
          if (json._trigger_supervisor !== undefined) {
            triggerSupervisor = json._trigger_supervisor;
            delete json._trigger_supervisor;
          }
          this.get('store').pushPayload(json);
          if (triggerSupervisor && this.get('supervisor')) {
            const to_update = [];
            let key;
            for (key in json) {
              to_update.push(key);
            }
            this.set('supervisor.changed_models', to_update);
          }
          // console.log('this is this', this, typeof this, this.constructor.name, this.get('supervisor'));
          this._handleMessage({_message: message});
          resolve(orig);
        }, (err) => {
          if (!this._handleMessage(err)) {
            this.get('flashMessages').danger(`An error occured while performing "${text}" ${onText}`);
          }
          reject(err);
        });
      });
    } else if (act.refresh === true) {
      return new Ember.RSVP.Promise((resolve, reject) => {
        rv.then((orig) => {
          const json = {...orig};
          let receiver = this.get('receiver');
          if (!receiver) {
            receiver = this;
          }
          let message = {
            type: 'success',
            value: `"${text}" performed successfully ${onText}`
          };
          if (json._message !== undefined) {
            message = json._message;
            delete json._message;
            if (message.type === 'error') {
              message.type = 'danger';
            }
          }
          receiver.filterModel(receiver.get('_search_value'), receiver.get('_filter_values'), 1).then(() => {
            resolve(orig);
          }, reject);
          this.get('flashMessages')[message.type](message.value);
        }, reject);
      });
    }

    return rv.then((orig) => {
      const json = {...orig};
      let message = {
        type: 'success',
        value: `"${text}" performed successfully ${onText}`
      };
      if (json._message !== undefined) {
        message = json._message;
        delete json._message;
        if (message.type === 'error') {
          message.type = 'danger';
        }
      }
      this.get('flashMessages')[message.type](message.value);
    }).catch((json) => {
      let message = {
        type: 'danger',
        value: `An error occured while performing "${text}" ${onText}`
      };
      if (json._message !== undefined) {
        message = json._message;
        delete json._message;
        if (message.type === 'error') {
          message.type = 'danger';
        }
      }
      this.get('flashMessages')[message.type](message.value);
    });

  },

  rqst: task(function * (url, act, model, isWizard) {
    const { text } = act;
    this.set('performing', text);
    yield this.doRqst(url, act, model, isWizard).finally(() => {
      this.set('performing', false);
    });
  }),

  _set_defaults_for_wizard(model, fieldsets) {
    const fields = Ember.get(fieldsets, 'fields');
    if (!fields) {
      return;
    }
    fields.forEach((field) => {
      if (field.hasOwnProperty('fields')) {
        this._set_defaults_for_wizard(model, field);
      } else if (field.hasOwnProperty('tabs')) {
        field.tabs.forEach((tab) => {
          this._set_defaults_for_wizard(model, tab);
        });
      } else {
        const prop = Ember.get(field, 'extra.default_property_path');
        if (prop) {
          model.set(Ember.get(field, 'name'), model.get(prop));
        } else {
          const def = this.get_default_for_field(field);
          if (def && def.isFulfilled !== undefined) {
            def.then((related) => {
              model.set(Ember.get(field, 'name'), related);
            });
          } else {
            model.set(Ember.get(field, 'name'), def);
          }
        }
      }
    });
  },

  _do_open_wizard(main, modelLoader, params, record, act) {
    main.set('wizardParams', [modelLoader.merge_fields_fieldset(params.fields, {fields: Ember.get(params, 'fieldsets')})]);
    const model =  this.get('store').createRecord(`wizard/${params.model}`, {});
    model.set('parent', record);
    this._set_defaults_for_wizard(model, {'fields': Ember.get(params, 'fieldsets')});
    main.set('wizardModel', model);
    main.set('wizardAct', act);
    main.set('wizardOpen', true);
    main.set('wizard_action', this.doRqst);
  },

  _wizard(record, act) {
    const modelLoader = this.get('modelLoader');
    const promises = [];
    const { params } = act;
    let main = this.get('main');
    if (main === undefined) {
      main = this;
    }
    params.needs.forEach((need) => {
      promises.push(modelLoader.ensure_model(need.app, need.singular, need.plural));
    });

    promises.push(modelLoader.ensure_model('wizard', params.model, params.model, false));
    Ember.RSVP.allSettled(promises).then(() => {
      try {
        this._do_open_wizard(main, modelLoader, params, record, act);
      } catch(err) {
        if (err.message && err.message.match('You need to pass a model name to the store\'s serializerFor method')) {
          Ember.run.next(this, () => { this._do_open_wizard(main, modelLoader, params, record, act); });
        }
      }
    }, (err) => {
      console.error(err);
      if (!this._handleMessage(err)) {
        this.get('flashMessages').danger(`An error occured while performing "${act.text}" on ${this.get('model.__str__')}`);
      }
    });
  },

  actions: {
    requestConfirm(record, act) {
      const confirm = Ember.get(act, 'confirm');
      if (!confirm) {
        return;
      }
      const actionName = Ember.get(act, 'text');
      this.set('confirmType', 'action');
      this.set('modalTitle', `Confirm ${actionName} on ${record.get('__str__')}`);
      if (confirm !== true) {
        this.set('modalMessage', confirm.replace(':name', record.get('__str__')).replace(':id', record.get('id')));
      } else {
        this.set('modalMessage', `Please confirm you want to perform "${actionName}" on ${record.get('__str__')}`);
      }
      this.set('modalRecord', record);
      this.set('modalField', undefined);
      const rv = new Ember.RSVP.Promise((resolve, reject) => {
        this.set('modalConfirmAction', resolve);
        this.set('modalCancelAction', reject);
      });
      this.set('modalPromise', rv);
      this.set('modalOpen', true);
      return rv;
    },

    resetConfirmTypeAndClose() {
      this.set('modalOpen', false);
      this.set('confirmType', null);
      this.set('modalMessae', undefined);
    },
  },
});
