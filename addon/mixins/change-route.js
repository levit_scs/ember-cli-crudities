import Ember from 'ember';
import ConfigurableLoader from './configurable-loader';

export default Ember.Mixin.create(ConfigurableLoader, {
  modelLoader: Ember.inject.service('modelLoader'),
  model(params) {
    const modelLoader = this.get('modelLoader');
    return new Ember.RSVP.Promise((resolve, reject) => {
      modelLoader.ensure_model(params.app_name, params.model_name).then((app_info) => {
        resolve({
          ...params,
          meta: app_info.meta
        });
      }).catch((err) => {
        reject(err);
      });
    });
  },
});
