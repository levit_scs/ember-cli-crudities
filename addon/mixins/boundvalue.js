import Ember from 'ember';

export default Ember.Mixin.create({

  original: undefined,

  translated: Ember.computed('field', 'field.translated', function() {
    return this.get('field.translated');
  }),

  didReceiveAttrs() {
    this.set('original', this.get(`model.${this.get('property')}`));
  },

  onValueChange: Ember.observer('value', function() {
    if (this.attrs.onChange !== undefined) {
      let original = this.get('original');
      try {
        original = original.get('id');
      } catch (e) {
        /* ok */
      }
      let value = this.get('value');
      try {
        value = value.get('id');
      } catch (e) {
        /* ok */
      }
      if (value !== original || value === undefined) {
        this.onChange(this.get('property'), this.get('value'));
      }
    }
  })
});
