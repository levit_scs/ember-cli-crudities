import Ember from 'ember';
import doFormat from '../utils/conditional-format';

export function conditionalFormat(params/*, hash*/) {
  const model = params[0];
  const conditions = params[1];
  return doFormat(model, conditions);
}

export default Ember.Helper.helper(conditionalFormat);
