import { helper } from '@ember/component/helper';

export function filterForField(params/*, hash*/) {
  if (params.length !== 2) {
    return '';
  }
  const filtered = params[1].filter((field) => {
    return field.field === params[0];
  });
  if (filtered.length > 0) {
    return filtered[0].filter;
  }
  return '';
}

export default helper(filterForField);
