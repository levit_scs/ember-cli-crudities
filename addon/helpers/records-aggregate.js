import Ember from 'ember';
import aggregate from '../utils/records-aggregates';

export function recordsAggregate(params/*, hash*/) {
  const { type, property_path, group_by } = params[0];
  const records = params[1];

  const rv = aggregate(records, type, property_path, group_by);

  if (group_by) {
    const rrv = [];
    for(const prop in rv) {
      if (rv.hasOwnProperty(prop)) {
        rrv.push(`${prop}: ${rv[prop][0]}`);
      }
    }
    return rrv;
  }

  return [rv['_']];
}

export default Ember.Helper.helper(recordsAggregate);
